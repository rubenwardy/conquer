local _sessions = {}

local function make_callback(name)
	local callbacks = {}
	conquer["register_on_" .. name] = function(func)
		table.insert(callbacks, func)
	end

	conquer["run_on_" .. name] = function(...)
		for i=1, #callbacks do
			callbacks[i](...)
		end
	end
end

local function make_returning_callback(name)
	local callbacks = {}
	conquer["register_on_" .. name] = function(func)
		table.insert(callbacks, func)
	end

	conquer["run_on_" .. name] = function(...)
		for i=1, #callbacks do
			local ret = callbacks[i](...)
			if ret ~= nil then
				return ret
			end
		end
	end
end

make_callback("building_placed")
make_callback("selected_units_changed")
make_callback("join_session")
make_callback("leave_session")
make_callback("change_country")
make_callback("train_unit")
make_returning_callback("can_train_unit")
make_callback("unit_created")
make_callback("building_destroyed")

local S = conquer.S

function conquer.create_session(owner, pos)
	local session = conquer.Session:new(owner, pos)
	_sessions[session.id] = session
	return session
end

function conquer.import_session(data)
	local session = conquer.Session:new(data)
	_sessions[session.id] = session
	return session
end

function conquer.delete_session(session)
	local players = conquer.get_players_in_session(session)
	for _, session_player in ipairs(players) do
		session:leave(session_player)
	end

	_sessions[session.id] = nil
end

function conquer.get_session_by_id(pos)
	assert(type(pos) == "string")
	return _sessions[pos]
end

function conquer.get_sessions()
	return _sessions
end

function conquer.place_keep(session, country, pos)
	if not session or not country then
		return false, S("You need to join a country")
	end

	assert(session.get_countries)
	assert(country.colors.primary)

	if country.keep ~= nil then
		return false, S("You've already placed a keep")
	end

	for _, other_country in pairs(session:get_countries()) do
		if other_country.keep and vector.distance(other_country.keep, pos) < 15 then
			return false, S("You're two close to @1's keep! You need to be at least @2 nodes away",
					other_country.description, 15)
		end
	end

	country.keep = vector.new(pos)

	conquer.run_on_building_placed(session, country, "keep")

	return true
end

function conquer.place_building(building_type, session, country, pos)
	local singleton = building_type == "barracks"

	if not session or not country then
		return false, S("You need to join a country")
	end

	assert(session.get_countries)
	assert(country.colors.primary)

	if country.keep == nil then
		return false, S("You need to place a keep")
	end

	if singleton and country[building_type] ~= nil then
		return false, S("This building may only be placed once")
	end

	if vector.distance(country.keep, pos) > 5 then
		return false, S("Buildings must be placed within 5 nodes of your keep")
	end

	if singleton then
		country[building_type] = vector.new(pos)
	end

	conquer.run_on_building_placed(session, country, building_type)

	return true
end

function conquer.get_players_in_session(session)
	local ret = {}
	for _, player in pairs(minetest.get_connected_players()) do
		if player:get_meta():get("conquer:session") == session.id then
			ret[#ret + 1] = player
		end
	end

	return ret
end

function conquer.get_session(player)
	assert(player.get_player_name, "get_session expects a player")

	local session_id = player:get_meta():get("conquer:session")
	return session_id and _sessions[session_id]
end

function conquer.get_session_country(player)
	assert(player.get_player_name, "get_session_country expects a player")

	local session = conquer.get_session(player)
	if not session then
		return nil, nil
	end

	local country = session:get_country_by_player(player:get_player_name())
	if not country then
		return nil, nil
	end

	return session, country
end

function conquer.can_train_unit(session, country, unit_type)
	local message = conquer.run_on_can_train_unit(session, country, unit_type)
	if message then
		return false, message
	end

	return true, nil
end

function conquer.train_unit(session, country, unit_type)
	conquer.run_on_train_unit(session, country, unit_type)
end
