local commands = {}
local S = conquer.S

minetest.register_privilege("conquer_mgr", {
	description = "Can manage sessions and countries",
	give_to_singleplayer = false,
})

function conquer.register_command(name, def)
	assert(not commands[name])
	assert(def.description)
	assert(def.func)

	def.name = name
	commands[name] = def
end

local function get_sessions_list(pos)
	local ret = {}

	local sessions = conquer.get_sessions()
	for _, session in pairs(sessions) do
		local players = conquer.get_players_in_session(session)
		if #players > 0 then
			for i=1, #players do
				players[i] = players[i]:get_player_name()
			end

			if pos then
				local dist = vector.distance(pos, session:get_pos())
				ret[#ret + 1] = (" - [%dm] %s"):format(dist, table.concat(players, ", "))
			else
				ret[#ret + 1] = (" - %s"):format(table.concat(players, ", "))
			end
		end
	end

	return ret
end

minetest.register_chatcommand("conquer", {
	func = function(name, param)
		for cmd_name, def in pairs(commands) do
			if param == cmd_name or param:sub(1, #cmd_name + 1) == cmd_name .. " " then
				local has_privs, missing_privs = minetest.check_player_privs(name, def.privs or {})
				if has_privs then
					return def.func(name, param:sub(#cmd_name + 2))
				else
					return false, S("Missing privileges: @1", table.concat(missing_privs, ", "))
				end
			end
		end

		local msg = {
			S("Usage:")
		}

		for cmd_name, def in conquer.orderedPairs(commands) do
			if not def.privs or minetest.check_player_privs(name, def.privs) then
				local command = cmd_name
				if def.params then
					command = command .. " " .. def.params
				end
				msg[#msg + 1] = " - " .. minetest.colorize("#9cf", command) .. ": " .. def.description
			end
		end

		return false, table.concat(msg, "\n")
	end
})


conquer.register_command("help", {
	description = "Show help",
	func = function(name)
		conquer.show_help(name)
		return true, "Showed help"
	end
})


conquer.register_command("list", {
	description = "List sessions",
	func = function(name)
		local player = minetest.get_player_by_name(name)
		local sessions = get_sessions_list(player and player:get_pos())
		if #sessions > 0 then
			return true, S("List of sessions:") .. "\n" .. table.concat(sessions, "\n")
		else
			return true, S("No sessions")
		end
	end
})

conquer.register_command("start", {
	description = "Start new session",
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		if conquer.get_session(player) then
			return false, S("You're already in a session")
		end

		local min_session_dist = tonumber(minetest.settings:get("conquer.min_session_distance")) or 75
		local sessions = conquer.get_sessions()
		for _, session in pairs(sessions) do
			if vector.distance(session:get_pos(), player:get_pos()) < min_session_dist then
				return false, S("You're too close to an existing session, created by @1. You need to be at least @2 nodes away.",
						session.owner, min_session_dist)
			end
		end

		local session = conquer.create_session(name, player:get_pos())
		local country = session:join(player)
		assert(country)

		return true, S("Started session as country @1", country.description)
	end,
})

conquer.register_command("leave", {
	description = "Leave current session",
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("You're not currently in a session")
		end

		session:leave(player)

		return true, S("Left session")
	end,
})

conquer.register_command("join", {
	description = "Join session",
	params = "<username>",
	func = function(name, target_name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		if target_name:trim() == "" then
			local sessions = get_sessions_list(player:get_pos())
			if #sessions > 0 then
				return true, S("List of sessions:") .. "\n" .. table.concat(sessions, "\n")
			else
				return true, S("No sessions")
			end
		end

		local target = minetest.get_player_by_name(target_name)
		if not target then
			return false, S("Player @1 is not online", target_name)
		end

		local dist = vector.distance(target:get_pos(), player:get_pos())
		if dist > 50 then
			return false, S("You need to be closer than 50 nodes")
		end

		local session = conquer.get_session(target)
		if not session then
			return false, S("Player @1 is not currently playing conquer", target_name)
		end

		if conquer.get_session(player) then
			return false, S("You're already in a session")
		end

		local country = session:join(player)
		if not country then
			return false, S("Can't join session as it is full")
		end

		return true, S("Joined @1's session as country @2", target_name, country.description)
	end
})

conquer.register_command("new_country", {
	description = "Create country",
	privs = { conquer_mgr = true },
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("There is no active session")
		end

		local country = session:create_country()
		if not country then
			return false, S("Unable to create country")
		end

		return true, S("Created new country: @1", country.description)
	end
})

conquer.register_command("join_country", {
	description = "Join country",
	params = "<country>",
	privs = { conquer_mgr = true },
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("There is no active session")
		end

		if not session:set_player_country(player, param) then
			return false, S("Unable to set the country, does it exist?")
		end

		return true, S("Joined country @1", param:trim())
	end
})

conquer.register_command("new_join", {
	description = "Create and join country",
	privs = { conquer_mgr = true },
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("There is no active session")
		end

		local country = session:create_country()
		if not country then
			return false, S("Unable to create country")
		end

		assert(session:set_player_country(player, country.id))

		return true, S("Joined new country @1", country.description)
	end
})

conquer.register_command("place", {
	description = "Create, join, and place keep and barracks for country",
	privs = { conquer_mgr = true },
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("There is no active session")
		end

		local country = session:create_country()
		if not country then
			return false, S("Unable to create country")
		end

		assert(session:set_player_country(player, country.id))

		local keep_pos = vector.round(vector.add(player:get_pos(), vector.new(0, 0.01, 0)))
		minetest.item_place_node(ItemStack("conquer:keep"), player, {
			type="node",
			under=vector.subtract(keep_pos, vector.new(0, 1, 0)),
			above=keep_pos,
		})

		local one = vector.new(1, 1, 1)
		local candidates = minetest.find_nodes_in_area_under_air(
			vector.subtract(keep_pos, one), vector.add(keep_pos, one),
			{"group:stone", "group:soil"})
		if #candidates == 0 then
			return false, S("Unable to place barracks")
		end

		minetest.item_place_node(ItemStack("conquer:barracks"), player, {
			type="node",
			under=candidates[1],
			above=vector.add(candidates[1], vector.new(0, 1, 0)),
		})

		return true, S("Created new country @1", country.description)
	end
})

conquer.register_command("give", {
	description = "Give resource to country",
	params = "[<country>] <stack>",
	privs = { give = true },
	func = function(name, param)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local session = conquer.get_session(player)
		if not session then
			return false, S("There is no active session")
		end

		local country, stack = param:match("^([A-Za-z]+) (.+)$")
		if country then
			country = session:get_country_by_id(country:trim())
		else
			country = session:get_country_by_player(name)
			stack = param
		end

		if not country then
			return false, S("Unable to find country")
		end

		stack = ItemStack(stack)
		local itemname = minetest.registered_aliases[stack:get_name()] or stack:get_name()
		if not minetest.registered_items[itemname] then
			return false, S("Unable to give unknown item @1", itemname)
		end

		if conquer.add_resource(country, stack) then
			return true, S("Gave @1 to @2", stack:get_description(), country.description)
		else
			return false, S("Unable to give @1 to @2", stack:get_description(), country.description)
		end
	end
})

conquer.register_command("delete_all", {
	description = "Delete all sessions",
	privs = { conquer_mgr = true },
	func = function(name)
		local player = minetest.get_player_by_name(name)
		if not player then
			return false, S("You need to be logged in!")
		end

		local sessions = conquer.get_sessions()
		for _, session in pairs(sessions) do
			conquer.delete_session(session)
		end

		return true, S("Deleted all sessions")
	end,
})
