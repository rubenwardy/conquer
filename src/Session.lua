local Session = conquer.class()
conquer.Session = Session

function Session:constructor(owner, pos)
	if type(owner) == "table" and not pos then
		self:from_table(owner)
	else
		assert(type(owner) == "string")

		pos = vector.floor(pos)
		self.id = minetest.pos_to_string(pos)
		self.pos = vector.new(pos)
		self.owner = owner
		self.counter = 0
		self.last_play_time = minetest.get_gametime()
		self._countries = {}
		self._countries_by_player = {}
		self._countries_by_id = {}
	end
end

function Session:to_table()
	return {
		id = self.id,
		pos = self.pos,
		owner = self.owner,
		counter = self.counter,
		last_play_time = self.last_play_time,
		countries = self._countries,
		countries_by_player = self._countries_by_player,
	}
end

function Session:from_table(data)
	self.id = data.id
	self.pos = data.pos
	self.owner = data.owner
	self.counter = data.counter
	self.last_play_time = data.last_play_time
	self._countries = data.countries
	self._countries_by_player = data.countries_by_player

	self._countries_by_id = {}
	for _, country in pairs(data.countries) do
		self._countries_by_id[country.id] = country
	end
end

function Session:get_countries()
	return self._countries
end

function Session:get_pos()
	return self.pos
end

local COUNTRIES = {
	{
		description = conquer.S("Red"),
		colors = {
			primary = "red",
			skin = "red",
			background = "#900",
			selection_disc = "#900",
		},
	},

	{
		description = conquer.S("Blue"),
		colors = {
			primary = "blue",
			skin = "blue",
			background = "#009",
			selection_disc = "#009",
		},
	},

	{
		description = conquer.S("Green"),
		colors = {
			primary = "green",
			skin = "green",
			background = "#090",
			selection_disc = "#090",
		},
	},

	{
		description = conquer.S("Purple"),
		colors = {
			primary = "purple",
			skin = "purple",
			background = "#8080ff",
			selection_disc = "#8080ff",
		},
	},

	{
		description = conquer.S("Yellow"),
		colors = {
			primary = "yellow",
			skin = "yellow",
			background = "#b6a941",
			selection_disc = "#b6a941",
		},
	},
}

function Session:create_country()
	if self.counter == #COUNTRIES then
		return nil
	end

	self.counter = self.counter + 1

	local country = table.copy(COUNTRIES[self.counter])
	country.id = country.colors.primary
	country.keep = nil
	country.players = {}

	table.insert(self._countries, country)
	self._countries_by_id[country.id] = country
	return country
end

function Session:remove_country(country)
	for pname, _ in pairs(country.players) do
		local player = minetest.get_player_by_name(pname)
		if player then
			self:leave(player)
		end
		self._countries_by_player[pname] = nil
	end

	self._countries_by_id[country.id] = nil
	for i=1, #self._countries do
		if self._countries[i].id == country.id then
			table.remove(self._countries, i)
			break
		end
	end

	return country
end

function Session:get_country_by_id(id)
	return self._countries_by_id[id]
end

function Session:get_country_by_player(pname)
	return self._countries_by_player[pname]
end

function Session:join(player)
	local pname = player:get_player_name()

	local meta = player:get_meta()
	assert(not meta:contains("conquer:session"))

	local country = self._countries_by_player[pname]
	if not country then
		country = self:create_country(pname)
		if not country then
			return nil
		end
	end

	meta:set_string("conquer:session", self.id)
	conquer.run_on_join_session(player, self)

	self:set_player_country(player, country.id)

	return country
end

function Session:leave(player)
	local pname = player:get_player_name()
	if self._countries_by_player[pname] then
		self._countries_by_player[pname].players[pname] = nil
	end

	local meta = player:get_meta()
	if meta:get("conquer:session") == self.id then
		meta:set_string("conquer:session", "")
		conquer.run_on_leave_session(player, self)
	end
end

function Session:set_player_country(player, country_id)
	local pname = player:get_player_name()

	local country = self._countries_by_id[country_id:lower():trim()]
	if not country then
		return false
	end

	local old = self._countries_by_player[pname]
	if old then
		old.players[pname] = nil
	end

	self._countries_by_player[pname] = country
	country.players[pname] = true

	conquer.run_on_change_country(player, self, country)

	return true
end

function Session:get_players_in_country(cname)
	local country = self:get_country_by_id(cname) or cname
	if type(country) ~= "table" then
		return nil
	end

	local ret = {}
	for name, _ in pairs(country.players) do
		ret[#ret + 1] = name
	end

	table.sort(ret)
	return ret
end
