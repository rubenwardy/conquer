-- This file, except the class API, is intentionally undocumented in the API reference for Conquer

local Object = {}
Object.__index = Object
function Object:new(...)
	local obj = {}
	setmetatable(obj, self)

	obj:constructor(...)

	return obj
end

function Object:constructor() --luacheck: ignore
end

function conquer.class(parent)
	parent = parent or Object
	assert(type(parent) == "table" and parent.new)

	local class_object = {
		constructor = function(self, ...)
			assert(#({...}) == 0, "Default constructor accepts no arguments. Define a constructor.")
			parent.constructor(self)
		end
	}
	class_object.__index = class_object

	setmetatable(class_object, parent)

	return class_object
end

function conquer.make_context_provider()
	local _contexts = {}

	if minetest.register_on_leaveplayer then
		minetest.register_on_leaveplayer(function(player)
			_contexts[player:get_player_name()] = nil
		end)
	end

	return function(player)
		local pname = player:get_player_name()
		local context = _contexts[pname] or {}
		_contexts[pname] = context
		return context
	end
end

function conquer.extend(base, def)
	for key, value in pairs(base) do
		if def[key] == nil then
			def[key] = value
		end
	end
	return def
end

--
-- Source: http://lua-users.org/wiki/SortedIteration
--

local function __genOrderedIndex( t )
	local orderedIndex = {}
	for key in pairs(t) do
		table.insert( orderedIndex, key )
	end
	table.sort( orderedIndex )
	return orderedIndex
end

function conquer.orderedNext(t, state)
	-- Equivalent of the next function, but returns the keys in the alphabetic
	-- order. We use a temporary ordered key table that is stored in the
	-- table being iterated.

	local key = nil
	if state == nil then
		-- the first time, generate the index
		t.__orderedIndex = __genOrderedIndex( t )
		key = t.__orderedIndex[1]
	else
		-- fetch the next value
		for i = 1,table.getn(t.__orderedIndex) do
			if t.__orderedIndex[i] == state then
				key = t.__orderedIndex[i+1]
			end
		end
	end

	if key then
		return key, t[key]
	end

	-- no more value to return, cleanup
	t.__orderedIndex = nil
	return
end

function conquer.orderedPairs(t)
	-- Equivalent of the pairs() function on tables. Allows to iterate
	-- in order
	return conquer.orderedNext, t, nil
end

function conquer.direction_to_rotation(direction)
	-- Thanks to LMD for help with this
	return {
		x = math.atan2(direction.y, math.sqrt(direction.z*direction.z+direction.x*direction.x)),
		y = -math.atan2(direction.x, direction.z),
		z = 0
	}
end

function conquer.calculate_projectile_direction(from, to, initial_speed, gravity)
	-- From https://en.wikipedia.org/wiki/Projectile_motion

	assert(type(from.x) == "number" and type(from.y) == "number" and type(from.z) == "number")
	assert(type(to.x) == "number" and type(to.y) == "number" and type(to.z) == "number")
	assert(type(initial_speed) == "number" and initial_speed > 0)
	assert(type(gravity) == "number" and gravity > 0)

	local delta = vector.subtract(to, from)
	local x = math.sqrt(delta.x*delta.x + delta.z*delta.z)
	local y = delta.y

	local x2 = x*x
	local v2 = initial_speed*initial_speed

	local square = v2*v2 - gravity*(gravity*x2 + 2*y*v2)
	if square <= 0 then
		return nil
	end

	local t_x = gravity * x
	local t_y = v2 - math.sqrt(square)

	local factor = t_x / x
	local u_y = t_y
	return vector.normalize(vector.new(delta.x * factor, u_y, delta.z * factor))
end

function conquer.find_empty_neighbour(pos)
	local neighbours = {
		vector.new(0, 0, 1),
		vector.new(1, 0, 0),
		vector.new(0, 0, -1),
		vector.new(-1, 0, 0),
		vector.new(1, 0, 1),
		vector.new(-1, 0, 1),
		vector.new(-1, 0, -1),
		vector.new(1, 0, -1),
	}

	for i=1, #neighbours do
		local new_pos = vector.add(pos, neighbours[i])
		local node = minetest.get_node(new_pos)
		local def = minetest.registered_nodes[node and node.name]
		if def and not def.walkable then
			return new_pos
		end
	end
	return nil
end

function conquer.find_path(start, target_pos, ...)
	return minetest.find_path(start, target_pos, ...) or
			minetest.find_path(vector.add(start, vector.new(0, 1, 0)), target_pos, ...)
end

function conquer.find_path_to_neighbour(start, target_pos, max_distance, jump_h, drop_h)
	local neighbours = {
		vector.new(-1, 0, 0),
		vector.new(0, 0, -1),
		vector.new(1, 0, 0),
		vector.new(0, 0, 1),
	}

	local min_path = nil
	local min_length = 0
	for i=1, #neighbours do
		local path = minetest.find_path(start,
			vector.add(target_pos, neighbours[i]), max_distance, jump_h, drop_h)
		local length = path and #path
		if path and (not min_path or length < min_length) then
			min_path = path
			min_length = length
		end
	end

	if min_path then
		min_path[min_length + 1] = target_pos
	end

	return min_path
end

function conquer.find_path_to_target(start, target)
	local target_pos = target:get_pos()
	if not target.is_node_target then
		return conquer.find_path(start, target_pos, 20, 1, 1)
	end

	return conquer.find_path_to_neighbour(start, target_pos, 20, 1, 1)
end

function conquer.line_of_sight(from, to, excludes)
	local raycast = minetest.raycast(from, to)
	for pointed_thing in raycast do
		if pointed_thing.type == "node" then
			local node = minetest.get_node(pointed_thing.under)
			local def = minetest.registered_nodes[node and node.name]
			if def and def.walkable then
				return false
			end
		elseif pointed_thing.type == "entity" then
			if not excludes or table.indexof(excludes, pointed_thing.ref) == -1 then
				return false
			end
		end
	end
	return true
end


-- Allows nodes to expose the same methods as an ObjectRef
function conquer.create_target(target)
	if target.is_node_target or target.get_pos then
		return target
	elseif target.x and target.y and target.z then
		return {
			is_node_target = true,

			get_pos = function()
				return vector.new(target)
			end,

			get_velocity = function()
				-- Nodes don't move :D
				return vector.new()
			end,

			punch = function(_, _, time_since_last_punch, tool_capabilities)
				conquer.damage_building(target, time_since_last_punch, tool_capabilities)
			end,

			get_hp = function()
				local def = minetest.registered_nodes[minetest.get_node(target).name]
				if not def or not def.damage then
					return
				end

				return def:get_hp(target)
			end,
		}
	else
		error("Unknown target!")
	end
end

function conquer.set_interval(interval, func, ...)
	local function run(...)
		func(...)
		minetest.after(interval, run, ...)
	end

	minetest.after(interval, run, ...)
end

function conquer.format_resource_list(resources)
	resources = table.copy(resources)
	for i=1, #resources do
		local stack = ItemStack(resources[i])
		resources[i] = conquer.S("@1x @2", stack:get_count(), stack:get_description())
	end
	return table.concat(resources, ", ")
end

function conquer.get_collision_box_height(pos)
	local node = minetest.get_node_or_nil(pos)
	if not node then
		return 0.5
	end

	local name = minetest.registered_aliases[node.name] or node.name
	local def = minetest.registered_nodes[name]
	local collision_box = def and (def.collision_box or def.nodebox)
	if not collision_box then
		return 0.5
	end

	if collision_box.type == "fixed" then
		local height = -0.5
		for i = 1, #collision_box.fixed do
			height = math.max(height, collision_box.fixed[i][5])
		end
		return height
	elseif collision_box.type ~= "regular" then
		minetest.log("warning",
			"[conquer.get_collision_box_height] Unimplemented collision_box type: " ..
				collision_box.type)
	end

	return 0.5
end
