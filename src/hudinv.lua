conquer.hud_position = {
	x = 0.5,
	y = 0.95
}

local function generate_number_texture(number)
	if number > 99 then
		number = 99
	end

	return ("([combine:22x22:0,0=conquer_%d.png:11,0=conquer_%d.png)"):format((number / 10) % 10, number % 10)
end

local function create_selection_info(units)
	assert(#units > 0)

	local info = {}
	if #units == 1 then
		local unit_def = units[1]:get_luaentity():get_unit_type()
		info.icon = unit_def.icon
		info.icon_scale = 3
		info.melee = unit_def.melee ~= nil
		info.ranged = unit_def.ranged ~= nil
	else
		info.icon = generate_number_texture(#units)
		info.icon_scale = 2
		info.melee = false
		info.ranged = false

		for i=1, #units do
			local unit_def = units[i]:get_luaentity():get_unit_type()
			info.melee = info.melee or unit_def.melee ~= nil
			info.ranged = info.ranged or unit_def.ranged ~= nil
		end
	end

	return info
end

local modes = {
	unit = {
		update = function(_, player, context, units)
			local info = create_selection_info(units)

			player:hud_set_hotbar_selected_image("conquer_gui_hotbar_unit_selected.png")
			player:hud_set_flags({
				wielditem = false,
			})

			if not context.bg then
				context.bg = player:hud_add({
					hud_elem_type = "image",
					position  = conquer.hud_position,
					offset    = {x = 0, y = 0},
					text      = "conquer_gui_hotbar_unit.png",
					scale     = { x = 1, y = 1},
					alignment = { x = 0, y = -1 },
					z_index   = -100,
				})
			end

			if not context.hotbar then
				context.hotbar = player:hud_add({
					hud_elem_type = "inventory",
					position  = conquer.hud_position,
					offset    = {x = -125, y = -70},
					text      = "main",
					number    = 3,
					item      = player:get_wield_index(),
					direction = 0
				})
			end

			if context.icon then
				player:hud_change(context.icon, "text", info.icon)
				player:hud_change(context.icon, "scale", { x = info.icon_scale, y = info.icon_scale })
			else
				context.icon = player:hud_add({
					hud_elem_type = "image",
					position  = conquer.hud_position,
					offset    = {x = -180 - 4, y = -70 + 64 - 5 - 9},
					text      = info.icon,
					scale     = { x = info.icon_scale, y = info.icon_scale },
					alignment = { x = 0, y = -1 },
					z_index   = -100,
				})
			end
		end,

		set_inv = function(_, player, inv, units)
			local itemcount = 1
			inv:set_list("main", {
				ItemStack("conquer:move"),
			})

			local info = create_selection_info(units)

			if info.melee then
				inv:add_item("main", "conquer:melee")
				itemcount = itemcount + 1
			end

			if info.ranged then
				inv:add_item("main", "conquer:ranged")
				itemcount = itemcount + 1
			end

			player:hud_set_hotbar_itemcount(itemcount)
		end,
	},

	build = {
		update = function(_, player, context)
			player:hud_set_hotbar_selected_image("conquer_gui_hotbar_build_selected.png")
			player:hud_set_flags({
				wielditem = true,
			})

			if not context.bg then
				context.bg = player:hud_add({
					hud_elem_type = "image",
					position  = conquer.hud_position,
					offset    = {x = 0, y = 0},
					text      = "conquer_gui_hotbar_build.png",
					scale     = { x = 1, y = 1},
					alignment = { x = 0, y = -1 },
					z_index   = -100,
				})
			end

			if not context.hotbar then
				context.hotbar = player:hud_add({
					hud_elem_type = "inventory",
					position  = conquer.hud_position,
					offset    = {x = -112, y = -70},
					text      = "main",
					number    = 4,
					item      = player:get_wield_index(),
					direction = 0
				})
			end
		end,

		set_inv = function(_, player, inv, _)
			inv:set_list("main", {
				ItemStack("conquer:box_select"),
				ItemStack("conquer:keep"),
				ItemStack("conquer:barracks"),
				ItemStack("conquer:farm"),
			})
			player:hud_set_hotbar_itemcount(4)
		end,
	},
}


local function remove_all_huds(player, context)
	for key, value in pairs(context) do
		if type(value) == "number" then
			player:hud_remove(value)
		end
		context[key] = nil
	end
end

local hud_context_provider = conquer.make_context_provider()

local function update_hud(player, _, units)
	local context = hud_context_provider(player)
	local mode = #units > 0 and "unit" or "build"

	-- Clear all elements on HUD change
	if context.mode and context.mode ~= mode then
		remove_all_huds(player, context)
	end

	-- Update inventory
	if context.mode ~= mode or mode == "unit" then
		modes[mode]:set_inv(player, player:get_inventory(), units)
	end

	context.mode = mode
	modes[mode]:update(player, context, units)
end

local function update_hotbar_selected_items()
	local players = minetest.get_connected_players()
	for i=1, #players do
		local player = players[i]
		local context = hud_context_provider(player)
		if context.hotbar then
			local hotbar_idx = player:get_wield_index()
			if hotbar_idx ~= context.hotbar_idx then
				player:hud_change(context.hotbar, "item", hotbar_idx)
				context.hotbar_idx = hotbar_idx
			end
		end
	end

	minetest.after(0.1, update_hotbar_selected_items)
end

minetest.after(0.1, update_hotbar_selected_items)

conquer.register_on_selected_units_changed(function(player, _, country, units)
	update_hud(player, country, units)
end)

conquer.register_on_change_country(function(player, _, country)
	update_hud(player, country, conquer.get_selected_units(player))
end)



--
-- Hide default stuff when in a session
--

local hud_builtin_defaults_context_provider = conquer.make_context_provider()

conquer.register_on_join_session(function(player)
	local context = hud_builtin_defaults_context_provider(player)
	context.hotbar_image = player:hud_get_hotbar_image()
	context.hotbar_selected_image = player:hud_get_hotbar_selected_image()
	context.itemcount = player:hud_get_hotbar_itemcount()
	context.flags = player:hud_get_flags()

	player:hud_set_hotbar_image("conquer_gui_hotbar.png")
	player:hud_set_hotbar_selected_image("")
	player:hud_set_flags({
		hotbar = false,
		healthbar = false,
		breathbar = false,
	})

	local inv = player:get_inventory()
	inv:set_size("mainbackup", inv:get_size("main"))
	inv:set_list("mainbackup", inv:get_list("main"))
	inv:set_size("main", 4)
	inv:set_list("main", {})
end)

conquer.register_on_leave_session(function(player)
	local context = hud_builtin_defaults_context_provider(player)

	player:hud_set_hotbar_image(context.hotbar_image)
	player:hud_set_hotbar_selected_image(context.hotbar_selected_image)
	player:hud_set_hotbar_itemcount(context.itemcount)
	player:hud_set_flags(context.flags)

	remove_all_huds(player, hud_context_provider(player))

	local inv = player:get_inventory()
	inv:set_size("main", inv:get_size("mainbackup"))
	inv:set_list("main", inv:get_list("mainbackup"))
	inv:set_size("mainbackup", 0)
end)



local old_item_drop = minetest.item_drop
minetest.item_drop = function(itemstack, player, pos)
	if player and conquer.get_session(player) then
		conquer.deselect_all_units(player)
		return itemstack
	else
		return old_item_drop(itemstack, player, pos)
	end
end
