local dir = minetest.get_modpath("conquer") .. "/src"

dofile(dir .. "/utils.lua")
dofile(dir .. "/Session.lua")
dofile(dir .. "/api.lua")
dofile(dir .. "/building_api.lua")
dofile(dir .. "/units/init.lua")
dofile(dir .. "/content/init.lua")

dofile(dir .. "/hudinv.lua")
dofile(dir .. "/gui.lua")
dofile(dir .. "/gui_help.lua")
dofile(dir .. "/chatcmd.lua")


local function chat_send_country(_, country, message, color)
	for pname, _ in pairs(country.players) do
		minetest.chat_send_player(pname, minetest.colorize(color or "#EE8822", message))
	end
end

local function chat_send_all(session, message, except_player, color)
	local players = conquer.get_players_in_session(session)
	for i=1, #players do
		if players[i] ~= except_player then
			minetest.chat_send_player(players[i]:get_player_name(),
					minetest.colorize(color or "#EE8822", message))
		end
	end
end

local S = conquer.S

local groups_context_provider = conquer.make_context_provider()

conquer.register_on_join_session(function(player, session)
	local message = S("@1 has joined the session", player:get_player_name())
	chat_send_all(session, message, player)

	local context = groups_context_provider(player)
	context.armor_groups = player:get_armor_groups()
	player:set_armor_groups({ immortal = 1 })

	context.nametag = player:get_nametag_attributes().color
end)

conquer.register_on_leave_session(function(player, session)
	local message = S("@1 has left the session", player:get_player_name())
	chat_send_all(session, message, player)

	conquer.deselect_all_units(player)

	local context = groups_context_provider(player)
	if context.armor_groups then
		player:set_armor_groups(context.armor_groups)
	end

	player:set_nametag_attributes({
		color = context.nametag
	})
end)

conquer.register_on_change_country(function(player, session, country)
	local message = conquer.S("@1 has joined country @2",
			player:get_player_name(), country.description)
	chat_send_all(session, message, player)

	conquer.deselect_all_units(player)

	player:set_nametag_attributes({
		color = country.colors.nametag or country.colors.primary
	})
end)

conquer.register_on_building_destroyed(function(session, country, building_type, _)
	if building_type == "keep" then
		local player_names = session:get_players_in_country(country)
		if #player_names > 0 then
			chat_send_all(session, conquer.S("@1 (@2) was defeated",
					country.description, table.concat(player_names, ",")))
		else
			chat_send_all(session, conquer.S("@1 was defeated",
					country.description))
		end

		session:remove_country(country)

		local countries = session:get_countries()
		if #countries == 1 then
			local winners = table.concat(
					session:get_players_in_country(countries[1]), ",")

			chat_send_all(session, conquer.S("@1 (@2) won!",
					countries[1].description, winners))
			chat_send_all(session,
					conquer.S("You can continue playing.") .. " " ..
					conquer.S("Perhaps get more people to join?"),
					nil, "#808080")
		else
			chat_send_all(session, conquer.S("@1 countries remaining",
					#countries), nil, "#808080")
		end
	end
end)

conquer.register_on_unit_created(function(session, country, unit)
	local message = conquer.S("Finished training @1", unit:get_luaentity():get_unit_type().description)
	chat_send_country(session, country, message, "#808080")
end)

minetest.register_on_joinplayer(function(player)
	local session_id = player:get_meta():get("conquer:session")
	player:get_meta():set_string("conquer:session", "")

	local inv = player:get_inventory()
	if inv:get_size("mainbackup") > 0 then
		inv:set_size("main", inv:get_size("mainbackup"))
		inv:set_list("main", inv:get_list("mainbackup"))
		inv:set_size("mainbackup", 0)
	end

	local session = session_id and conquer.get_session_by_id(session_id)
	if session and session:join(player) then
		return
	end
end)

local function update_session(session)
	local destroy_time = tonumber(minetest.settings:get("conquer.session_idle_timeout")) or 60

	local current_game_time = minetest.get_gametime()
	local time = current_game_time - session.last_play_time
	if time < 0 or
			#conquer.get_players_in_session(session) > 0 then
		session.last_play_time = current_game_time
	elseif destroy_time > 0 and time > destroy_time then
		conquer.delete_session(session)
		return
	end

	for _, country in pairs(session:get_countries()) do
		if country.keep then
			conquer.heal_building(country.keep)
		end

		if country.barracks then
			conquer.heal_building(country.barracks)
		end
	end
end

conquer.set_interval(3, function()
	for _, session in pairs(conquer.get_sessions()) do
		update_session(session)
	end
end)

local old_format_chat_message = minetest.format_chat_message
minetest.format_chat_message = function(name, message)
	local player = minetest.get_player_by_name(name)
	if not player then
		return old_format_chat_message(name, message)
	end

	local _, country = conquer.get_session_country(player)
	if not country then
		return old_format_chat_message(name, message)
	end

	local color = country.colors.chat or country.colors.primary
	return minetest.colorize(color, "<" .. name .. ">") .. " " .. message
end
