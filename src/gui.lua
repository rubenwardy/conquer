--
-- Formspec Generation Utilities
--

local function make_image_button(def)
	assert(type(def.x) == "number")
	assert(type(def.y) == "number")
	assert(type(def.w) == "number")
	assert(type(def.h) == "number")
	assert(not def.name or type(def.name) == "string")
	assert(type(def.image) == "string")
	assert(not def.label or type(def.label) == "string")
	if def.enabled == nil then
		def.enabled = true
	end
	assert(type(def.enabled) == "boolean")

	if def.enabled then
		return ("image_button[%f,%f;%f,%f;%s;%s;%s]"):format(
			def.x, def.y, def.w, def.h,
			minetest.formspec_escape(def.image or ""),
			def.name or "",
			minetest.formspec_escape(def.label or "")
		)
	else
		return ("box[%f,%f;%f,%f;#111]image[%f,%f;%f,%f;%s]label[%f,%f;%s]"):format(
			def.x, def.y, def.w, def.h,
			def.x, def.y, def.w, def.h,
			minetest.formspec_escape(def.image .. "^[opacity:90"),
			def.x + 0.1, def.y + def.h / 2,
			minetest.formspec_escape(def.label or "")
		)
	end
end

local function FS(...)
	return minetest.formspec_escape(conquer.S(...))
end


--
-- Keep GUI
--

local header = [[
	formspec_version[3]
	size[5.75,5]
]]

function conquer.make_keep_gui(_, session, country)
	local help = ([[
		style[help;border=false]
		box[4.75,0;1,0.5;#00000022]
		button[4.75,0;1,0.5;help;%s]
	]]) :format(FS("Help"))

	if not country then
		return help .. "label[0.375,0.5;" .. FS("Please join a country") .. "]"
	elseif not country.keep then
		return help .. "label[0.375,0.5;" .. FS("You need to place a keep") .. "]"
	end

	local inv_key = ("nodemeta:%d,%d,%d"):format(country.keep.x, country.keep.y, country.keep.z)

	local fs = {
		"box[0,0;5.75,0.5;", country.colors.background, "]",
		"style[banner;border=false]",
		"button[0,0;5.75,0.5;banner;", minetest.formspec_escape(country.description), "]",

		help,

		"box[0,0.5;5.75,0.5;#222]",
		"style[banner;border=false]",
		"button[0,0.5;5.75,0.5;banner;", FS("Stockpile"), "]",
		"list[", inv_key, ";stockpile;0.375,1.375;4,1]",

		"box[0,2.75;6,0.5;#222]",
		"style[banner2;border=false]",
		"button[0,2.75;5.75,0.5;banner2;", FS("Train Units"), "]",

		"container[0.375,3.625]",
	}

	do
		local button = {
			x = 0, y = 0, w = 1, h = 1
		}

		for _, unit in pairs(conquer.get_unit_types()) do
			local craftable, message = conquer.can_train_unit(session, country, unit)

			button.name = "unit_" .. unit.name
			button.image = unit.icon
			button.enabled = craftable

			fs[#fs + 1] = make_image_button(button)

			local tooltip = unit.description
			tooltip = tooltip .. "\n" .. conquer.format_resource_list(unit.craft.resources)

			if message then
				local color = craftable and "green" or "red"
				tooltip = tooltip .. "\n\n" ..
					minetest.colorize(color, message)
			end

			fs[#fs + 1] = ("tooltip[%f,%f;%f,%f;%s]"):format(
				button.x, button.y, button.w, button.h,
				minetest.formspec_escape(tooltip))

			button.x = button.x + button.w + 0.25
		end
	end

	fs[#fs + 1] = "container_end[]"

	return table.concat(fs, "")
end

-- Returns true to reshow the formspec
local function handle_submit(player, fields)
	local session, country = conquer.get_session_country(player)
	if not session or not country then
		return false
	end

	if fields.help then
		conquer.show_help(player:get_player_name())
		return false
	end

	for _, unit_type in pairs(conquer.get_unit_types()) do
		if fields["unit_" .. unit_type.name] then
			if conquer.can_train_unit(session, country, unit_type) then
				conquer.train_unit(session, country, unit_type)
			end
			return true
		end
	end
end


--
-- Raw formspecs
--
do
	function conquer.show_keep_gui(player)
		local session, country = conquer.get_session_country(player)
		local formspec = header .. conquer.make_keep_gui(player, session, country)
		minetest.show_formspec(player:get_player_name(), "conquer:keep", formspec)
	end

	minetest.register_on_player_receive_fields(function(player, formname, fields)
		if formname == "conquer:keep" and handle_submit(player, fields) then
			conquer.show_keep_gui(player)
		end
	end)
end




--
-- SFINV
--
local update_inventory_gui
if minetest.global_exists("sfinv") then
	local function wrap_page(page)
		local old_is_in_nav = page.is_in_nav or
				function() return true end

		page.is_in_nav = function(self, player)
			if conquer.get_session(player) then
				return false
			else
				return old_is_in_nav(self, player)
			end
		end
	end

	for _, page in pairs(sfinv.pages) do
		wrap_page(page)
	end


	sfinv.register_page("conquer:keep", {
		title = "Conquer",

		is_in_nav = function(_, player)
			return conquer.get_session_country(player) ~= nil
		end,

		get = function(_, player, context)
			local session, country = conquer.get_session_country(player)
			local formspec = conquer.make_keep_gui(player, session, country)
			return sfinv.make_formspec(player, context, formspec, false, header)
		end,

		on_player_receive_fields = function(_, player, context, fields)
			if handle_submit(player, fields) then
				sfinv.set_player_inventory_formspec(player, context)
			end
		end,
	})

	update_inventory_gui = function(player)
		local context = sfinv.get_or_create_context(player)
		if conquer.get_session_country(player) then
			sfinv.set_page(player, "conquer:keep")
		elseif context.page == "conquer:keep" then
			sfinv.set_page(player, sfinv.get_homepage_name(player))
		end
	end

	local old_get_homepage_name = sfinv.get_homepage_name
	sfinv.get_homepage_name = function(player)
		if conquer.get_session(player) then
			return "conquer:keep"
		else
			return old_get_homepage_name(player)
		end
	end

	local old_register_page = sfinv.register_page
	sfinv.register_page = function(name, page, ...)
		wrap_page(page)
		return old_register_page(name, page, ...)
	end

else
	update_inventory_gui = function(player)
		local session, country = conquer.get_session_country(player)
		local formspec = header .. conquer.make_keep_gui(player, session, country)
		player:set_inventory_formspec(formspec)
	end

	minetest.register_on_joinplayer(update_inventory_gui)

	minetest.register_on_player_receive_fields(function(player, formname, fields)
		if formname == "" and handle_submit(player, fields) then
			update_inventory_gui(player)
		end
	end)
end

local function update_inventory_gui_for_country(_, country)
	for pname, _ in pairs(country.players) do
		local player = minetest.get_player_by_name(pname)
		if player then
			update_inventory_gui(player)
		end
	end
end
conquer.register_on_join_session(update_inventory_gui)
conquer.register_on_leave_session(update_inventory_gui)
conquer.register_on_change_country(update_inventory_gui)
conquer.register_on_building_placed(update_inventory_gui_for_country)
conquer.register_on_unit_created(update_inventory_gui_for_country)

conquer.set_interval(5, function()
	for _, player in ipairs(minetest.get_connected_players()) do
		if conquer.get_session(player) then
			update_inventory_gui(player)
		end
	end
end)
