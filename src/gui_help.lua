local help_markup = [[
	<tag name=code color=#7bf font=mono>
	<b>Conquer</b> is a mod that adds RTS gameplay. It allows players to start
	Conquer sub-games, where they can place buildings, train units, and
	fight other players.

	<br>
	<big>Starting or joining a session</big>

	You can list running sessions by typing:

	<code>/conquer list</code>

	Start a new conquer game by typing:

	<code>/conquer start</code>

	Or join an existing session by typing:

	<code>/conquer join playername</code>

	You'll switch into Conquer playing mode, where you will be given buildings that you can place.
	You'll need to place a keep, which you must protect at all costs.

	You may leave a game and return to normal playing mode at anytime by typing:

	<code>/conquer leave</code>

	You can then rejoin the session later, if you wish.

	<br>
	<big>Conquer GUI</big>

	The Conquer GUI is the central place for monitoring your kingdom.
	Once in a session, you can view it by pressing the inventory key (I),
	or by punching/right-clicking the keep node.

	<br>
	<big>Recruiting Units</big>

	You'll need to place a barracks to be able to train new units.
	Once you have done that, open the Conquer GUI and select a unit to train.

	Training a unit requires time and resources.
	The barracks will start training the unit as soon as you select it,
	and will spawn the unit nearby when complete.

	You can obtain more food by placing farms.

	<br>
	<big>Controlling Units</big>

	Select a unit by left clicking.
	You can select multiple units using shift+click.
	You can deselect units by pressing Q or clicking somewhere else.

	Right-click to command the unit to do something.

	Change the active wand by scrolling or pressing a number - just like with normal hotbars.

	- <b>Move:</b> right-click on a node to move the unit there.<br>
	- <b>Melee Attack:</b> right-click on a unit or building to move the unit there, and attack.<br>
	- <b>Ranged Attack:</b> right-click on a unit to fire arrows. The unit must be in
	sight, and must be less than 10 nodes and more than 2 nodes away.

	Attacking buildings will damage them. Damaged buildings will be repaired
	automatically when there are no enemies nearby.

	<br>
	<big>Winning</big>

	You win the game by being the last surviving country.
	To defeat another kingdom, you will need to destroy their keep.

	You can destroy an enemy's barracks to prevent them from training troops.
]]

-- Hack: allow paragraphs to break over multiple lines, fix paragraph distancing.
help_markup = help_markup
		:gsub("\n\n", "<br>")
		:gsub("\n", "")
		:gsub("<br>", "\n")

-- Yes, I know this is not the correct way to escape markup
-- But given that the help is static, this is fine for now.
local help_plain = help_markup:gsub("<[^>]+>", ""):trim()

local mtver_warning = conquer.S("Conquer works best in Minetest 5.2 or later. Update now.")
local show_again = conquer.S("Tip: you can see this at any time by typing @1",
		minetest.colorize("#7bf", " /conquer help"))

local header =  [[
		formspec_version[3]
		size[9.5,10.75]
	]]

function conquer.make_help_formspec(name)
	local fs = header .. [[
		image[0,0;9.5,1.96875;conquer_help_banner.jpg]
		style[exit;border=false]
		box[9,0;0.5,0.5;#000000aa]
		button_exit[9,0;0.5,0.5;exit;x]
		container[0,1.96875]
	]]

	local info = minetest.get_player_information(name)
	if info and info.formspec_version >= 3 then
		fs = fs .. "hypertext[0.25,0.25;9,7.75;;" .. minetest.formspec_escape(help_markup) .. "]"
	else
		fs = fs .. "box[0,0;9.5,0.5;#f00]"
		fs = fs .. "label[0.2,0.25;" .. minetest.formspec_escape(mtver_warning) .. "]"
		fs = fs .. "textarea[0.25,0.75;9,7.25;;;" .. minetest.formspec_escape(help_plain) .. "]"
	end

	fs = fs .. "container_end[]"
	fs = fs .. "box[0,10.25;9.5,0.5;#00000033]"
	fs = fs .. "label[0.3,10.5;" .. show_again .. "]"

	return fs
end


function conquer.show_help(name)
	local formspec = conquer.make_help_formspec(name)
	minetest.show_formspec(name, "conquer:help", formspec)
end


--
-- On joinplayer
--
if minetest.settings:get_bool("conquer.show_help_on_join", false) then
	minetest.register_on_joinplayer(function(player)
		minetest.after(0.1, conquer.show_help, player:get_player_name())
	end)
end


--
-- SFINV
--
if minetest.global_exists("sfinv") and minetest.settings:get_bool("conquer.show_help_in_inventory", false) then
	sfinv.register_page("conquer:help", {
		title = "Conquer Help",

		get = function(_, player, context)
			local formspec = conquer.make_help_formspec(player:get_player_name())
			return sfinv.make_formspec(player, context, formspec, false, header)
		end,
	})
end
