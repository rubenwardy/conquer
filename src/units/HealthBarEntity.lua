local HealthBarEntity = {
	conquer = true,
	initial_properties = {
		physical = false,
		collide_with_objects = false,
		pointable = false,
		visual = "sprite",
		collisionbox = {x = 0, y = 0, z = 0},
		visual_size = {x = 1.5*0.4, y = 0.09375*0.4, z = 1.5*0.4},
		static_save = false,
	},
}

function HealthBarEntity:update_health(health, hp_max)
	local idx = math.floor(20 * health / hp_max)
	self.object:set_properties({
		textures = {
			("health_%d.png"):format(idx),
		},
	})
end

function HealthBarEntity:on_activate()
	self.object:set_armor_groups({ immortal = 1 })
end

function HealthBarEntity:on_detach()
	self.object:remove()
end

minetest.register_entity("conquer:healthbar", HealthBarEntity)
