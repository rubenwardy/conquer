local SelectionEntity = {
	conquer = true,
	initial_properties = {
		physical = false,
		collide_with_objects = false,
		pointable = false,
		collisionbox = {-0.5, 0.05, -0.5, 0.5, 0.1, 0.5},
		visual_size = {x = 8, y = 2 },
		mesh = "conquer_selection_disc.obj",
		textures = { "conquer_selection_disc.png" },
		visual = "mesh",
		static_save = false,
		use_texture_alpha = true,
	},
}

function SelectionEntity:set_color(color, ratio)
	self.object:set_properties({
		textures = {
			("conquer_selection_disc.png^[colorize:%s:%d"):format(color, ratio or 200),
		},
	})
end

function SelectionEntity:on_activate()
	self.object:set_armor_groups({ immortal = 1 })
end

function SelectionEntity:on_detach()
	self.object:remove()
end

minetest.register_entity("conquer:selection", SelectionEntity)


local UnitEntity = {
	conquer = true,
	initial_properties = {
		mesh = "conquer_humanlike.b3d",
		textures = {},
		visual = "mesh",
		visual_size = {x = 0.5, y = 0.5},
		collisionbox = {-0.15, 0.0, -0.15, 0.15, 0.85, 0.15},
		stepheight = 1,
		physical = true,
		collide_with_objects = false,
	},
}

function UnitEntity:get_unit_type()
	return assert(conquer.get_unit_type(self.unit_type))
end

function UnitEntity:setup(unit_type, session, country)
	assert(type(unit_type) == "string")
	assert(type(session) == "table")
	assert(type(country) == "table")

	self.unit_type = unit_type
	self.session_id = session.id
	self.country_id = country.id
	self.selection_entity = nil
	self.action = nil
	self.last_player_click = 0
	self.damage_sound_timer = 0

	local unit_def = self:get_unit_type()
	self.object:set_properties(unit_def.get_properties(session, country))
	if unit_def.animations and unit_def.animations.stand then
		self.object:set_animation(unit_def.animations.stand, 30, 0)
	end

	self.object:set_armor_groups(unit_def.armor_groups)

	if self.was_new then
		conquer.run_on_unit_created(session, country, self.object)

		self.object:set_hp(unit_def.max_hp)
	end

	self:update_healthbar()
end

function UnitEntity:select(session, country)
	assert(type(session) == "table")
	assert(type(country) == "table")

	if session.id ~= self.session_id or country.id ~= self.country_id then
		return false
	end

	self:deselect()

	self.selection_entity = minetest.add_entity(self.object:get_pos(), "conquer:selection")
	self.selection_entity:set_attach(self.object, "", vector.new(0, 0, 0), vector.new(0, 0, 0))
	self.selection_entity:get_luaentity():set_color(country.colors.selection_disc)

	return true
end

function UnitEntity:deselect()
	if self.selection_entity and self.selection_entity:get_luaentity() then
		self.selection_entity:remove()
		self.selection_entity = nil
	end
end

function UnitEntity:set_action(action)
	action:set_object(self.object)
	self.action = action
end

function UnitEntity:on_activate(staticdata)
	self.object:set_velocity(vector.new())

	if staticdata ~= "" and staticdata ~= nil then
		local data = minetest.parse_json(staticdata) or {}
		if not data.unit_type or not data.session_id or not data.country_id then
			self.object:remove()
			return
		end

		local session = conquer.get_session_by_id(data.session_id)
		local country = session and session:get_country_by_id(data.country_id)
		if not country then
			self.object:remove()
			return
		end

		self:setup(data.unit_type, session, country)
	else
		self.was_new = true
	end
end

function UnitEntity:get_session_country()
	local session = conquer.get_session_by_id(self.session_id)
	local country = session and session:get_country_by_id(self.country_id)
	if not country then
		self.object:remove()
		return nil, nil
	end

	return session, country
end

function UnitEntity:get_staticdata()
	return minetest.write_json({
		unit_type  = self.unit_type,
		session_id = self.session_id,
		country_id = self.country_id,
	})
end

assert(conquer.behaviour.states.RUNNING == "running")

function UnitEntity:on_step(delta)
	self.last_player_click  = self.last_player_click + delta
	self.damage_sound_timer = self.damage_sound_timer + delta

	if not self:get_session_country() then
		return
	end

	if self.action then
		self.action:on_step(delta)

		if self.action:get_state() ~= conquer.behaviour.states.RUNNING then
			local def = self:get_unit_type()
			self.object:set_velocity(vector.new())
			self.object:set_animation(def.animations.stand, 30, 0)
			self.action = nil
		end
		return
	end

	local def = self:get_unit_type()

	-- Find melee targets
	if def.melee then
		local targets = conquer.find_enemy_units(self.object:get_pos(),
				self.session_id, self.country_id, 0, 1.1)
		if #targets > 0 then
			local action = conquer.behaviour.MeleeAttack:new(targets[1], def.melee, def.animations)
			self:set_action(action)
			return
		end
	end

	-- Find ranged targets
	if def.ranged then
		local targets = conquer.find_enemy_units(self.object:get_pos(),
				self.session_id, self.country_id,
				def.ranged.min_range, def.ranged.max_range)
		for i=1, #targets do
			local action = conquer.behaviour.RangedAttack:new(targets[i], def.ranged, def.animations)
			action:set_object(self.object)
			if action:is_valid() then
				self:set_action(action)
				return
			end
		end
	end
end

function UnitEntity:play_damage_sound()
	if self.damage_sound_timer < 0.1 then
		return
	end

	minetest.sound_play({ name = "player_damage" }, {
		pos = self.object:get_pos(),
		gain = 0.25,
		max_hear_distance = 32,
	}, true)

	self.damage_sound_timer = 0
end

function UnitEntity:update_healthbar()
	local def = self:get_unit_type()
	local hp = self.object:get_hp()
	local needs_health_bar = hp < def.max_hp

	if needs_health_bar then
		if not self.healthbar or not self.healthbar:get_pos() then
			self.healthbar = minetest.add_entity(self.object:get_pos(), "conquer:healthbar")
			self.healthbar:set_attach(self.object, "", vector.new(0, 19, 0), vector.new(0, 0, 0))
		end
		self.healthbar:get_luaentity():update_health(hp, def.max_hp)
	elseif self.healthbar then
		self.healthbar:remove()
		self.healthbar = nil
	end
end

function UnitEntity:on_punch(puncher, _, _, _, damage)
	if puncher and puncher:is_player() then
		-- There will be multiple punches if wield item changes whilst punching
		if self.last_player_click < 0.1 then
			return true
		end

		self.last_player_click = 0

		if puncher:get_player_control().sneak then
			if self.selection_entity ~= nil then
				conquer.deselect_unit(puncher, self.object)
			else
				conquer.select_unit(puncher, self.object)
			end
		else
			conquer.replace_select_unit(puncher, self.object)
		end

		return true
	end

	local puncher_entity = puncher and puncher:get_luaentity()
	if not puncher_entity or not puncher_entity.conquer then
		return true
	end

	if damage > 0 then
		self:play_damage_sound()
		self:update_healthbar()
	end

	if puncher_entity.get_unit_type then
		local def = self:get_unit_type()
		if not self.action and def.melee then
			local action = conquer.behaviour.MeleeAttack:new(puncher, def.melee, def.animations)
			self:set_action(action)
		end
	end

	return false
end

function UnitEntity:on_death()
	minetest.after(0, function()
		local session = conquer.get_session_by_id(self.session_id)
		local players = conquer.get_players_in_session(session)
		for i=1, #players do
			-- This will deselect if invalid
			conquer.get_selected_units(players[i])
		end
	end)
end

minetest.register_entity("conquer:unit", UnitEntity)


function conquer.find_enemy_units(pos, session_id, country_id, min_range, max_range)
	local ret = {}
	local objects = minetest.get_objects_inside_radius(pos, max_range)
	for i=1, #objects do
		local object = objects[i]
		local entity = object:get_luaentity()
		if entity and entity.session_id == session_id and entity.country_id ~= country_id then
			local distance = vector.distance(pos, object:get_pos())
			if distance > min_range then
				-- Prefer low health nearby units
				ret[#ret + 1] = {
					weighting = math.min(10, object:get_hp()) + distance,
					ref = object,
				}
			end
		end
	end

	table.sort(ret, function(a, b) return a.weighting < b.weighting end)

	for i=1, #ret do
		ret[i] = ret[i].ref
	end

	return ret
end
