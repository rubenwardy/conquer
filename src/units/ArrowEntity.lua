local ArrowEntity = {
	conquer = true,
	initial_properties = {
		hp_max = 1,
		physical = false,
		collide_with_objects = true,
		collisionbox = {0,0,0, 0,0,0},
		pointable = false,

		visual = "mesh",
		mesh = "conquer_arrow.b3d",
		visual_size = {x=0.5, y=0.5},
		textures = {
			"conquer_arrow_head.png^conquer_arrow_uv.png^[makealpha:255,126,126"
		},

		static_save = false,
	},
}

-- Slightly less than realistic, as a lazy way to account for drag
local GRAVIY = 9

local INITIAL_SPEED = 10

function ArrowEntity:setup(caps, target_pos)
	assert(caps and caps.full_punch_interval)

	local pos = self.object:get_pos()

	local direction =
			conquer.calculate_projectile_direction(pos, target_pos, INITIAL_SPEED, GRAVIY)
	if not direction then
		return false
	end

	local velocity = vector.multiply(direction, INITIAL_SPEED)
	self.object:set_velocity(velocity)

	self.tool_capabilities = caps
	self.object:set_velocity(velocity)
	self:face_direction(direction)

	self.object:set_pos(vector.add(pos, vector.multiply(direction, 0.3)))
	return true
end

function ArrowEntity:face_direction(direction)
	local rotation = conquer.direction_to_rotation(direction)
	rotation.x = rotation.x + math.pi / 2
	self.object:set_rotation(rotation)
end

function ArrowEntity:on_activate()
	self.active = true
	self.lifetime = nil

	self.object:set_armor_groups({ immortal = 1 })
	self.object:set_acceleration(vector.new(0, -GRAVIY, 0))
end

function ArrowEntity:on_hit(pointed_thing)
	self.active = false
	self.lifetime = 4

	self.object:set_pos(pointed_thing.intersection_point)
	self.object:set_velocity(vector.new())
	self.object:set_acceleration(vector.new())

	self.object:set_properties({
		pointable = true,
		collisionbox = { -1/8,-1/8,-1/8, 1/8,1/8,1/8},
	})

	if pointed_thing.type == "object" then
		local caps = self.tool_capabilities
		pointed_thing.ref:punch(self.object, caps.full_punch_interval, caps)
	end
end

function ArrowEntity:on_punch()
	if not self.active then
		self.object:remove()
	end
end

function ArrowEntity:on_step(delta)
	if not self.active then
		self.lifetime = self.lifetime - delta
		if self.lifetime then
			self.object:remove()
		end
		return
	end

	local dir = vector.normalize(self.object:get_velocity())
	local from = self.object:get_pos()
	local to = vector.add(from, vector.multiply(dir, delta))
	self:face_direction(dir)

	local pointed_thing = minetest.raycast(from, to):next()
	if pointed_thing and pointed_thing.type ~= "nothing" then
		self:on_hit(pointed_thing)
	end
end

minetest.register_entity("conquer:arrow", ArrowEntity)
