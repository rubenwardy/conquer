local Node = conquer.behaviour.Node
local states = conquer.behaviour.states


local function debug(...) -- luacheck: ignore
	-- minetest.log("warning", ...)
end


local MeleeAttack = conquer.class(Node)
conquer.behaviour.MeleeAttack = MeleeAttack

function MeleeAttack:constructor(target, melee_spec, animations)
	self.target = conquer.create_target(target)
	self.melee_spec = melee_spec
	self.animations = animations
	self.recharge = 0
	self.reach = 1
	self.failed = false

	if target.is_node_target then
		self.reach = self.reach + 0.5
	end

	assert(self.animations.stand, "Needs stand animation")
	assert(self.animations.mine, "Needs mine animation")

	self.animation_time = nil
end

function MeleeAttack:get_state()
	if self.failed then
		return states.FAILED
	elseif self.target and self.target:get_pos() then
		return states.RUNNING
	else
		return states.SUCCESS
	end
end

function MeleeAttack:reset_animation()
	if self.animation_time then
		self.object:set_animation(self.animations.stand, 30, 0)
		self.animation_time = nil
	end
end

function MeleeAttack:on_step(dtime)
	if not self.target:get_pos() then
		self:reset_animation()
		return
	end

	local pos = self.object:get_pos()
	local target_pos = self.target:get_pos()
	if vector.distance(pos, target_pos) > self.reach then
		debug("MeleeAttack failed due to out of range")
		self:reset_animation()
		self.failed = true
		return
	end

	local delta = vector.subtract(target_pos, pos)
	self.object:set_yaw(math.atan2(delta.z, delta.x) - math.pi / 2)

	-- Reset animation and punch target when counter runs out
	if self.animation_time then
		if self.animation_time < 0 then
			self:reset_animation()

			local caps = self.melee_spec.tool_capabilities
			self.target:punch(self.object, caps.full_punch_interval, caps)

			-- Entity has died!
			if not self.target:get_hp() or self.target:get_hp() <= 0 then
				self.target = nil
				return
			end
		else
			self.animation_time = self.animation_time - dtime
		end
	end

	-- Start hit when recharged
	self.recharge = self.recharge - dtime
	if self.recharge < 0 then
		self.recharge = self.recharge + self.melee_spec.tool_capabilities.full_punch_interval

		local animation = self.animations.mine
		self.object:set_animation(animation, 30, 0)
		self.animation_time = (animation.y - animation.x) / 30
	end
end


local RangedAttack = conquer.class(Node)
conquer.behaviour.RangedAttack = RangedAttack

function RangedAttack:constructor(target, ranged_spec, animations)
	self.target = conquer.create_target(target)
	self.ranged_spec = ranged_spec
	self.animations = animations
	self.recharge = math.random() * self.ranged_spec.tool_capabilities.full_punch_interval
	self.state = states.RUNNING

	assert(self.animations.stand, "Needs stand animation")
	assert(self.animations.mine, "Needs mine animation")

	self.animation_time = nil
end

function RangedAttack:get_state()
	return self.state
end

function RangedAttack:is_valid()
	if not self.target:get_pos() then
		return false
	end

	local pos = self.object:get_pos()
	local target_pos = self.target:get_pos()
	local distance = vector.distance(pos, target_pos)

	if distance > self.ranged_spec.max_range or distance < self.ranged_spec.min_range then
		return false
	end

	return true
end

function RangedAttack:reset_animation()
	if self.animation_time then
		self.object:set_animation(self.animations.stand, 30, 0)
		self.animation_time = nil
	end
end

function RangedAttack:on_step(dtime)
	local target_pos = self.target:get_pos()
	if not target_pos or self.target:get_hp() <= 0 then
		self:reset_animation()
		self.state = states.SUCCESS
		return
	end

	local pos = self.object:get_pos()
	local distance = vector.distance(pos, target_pos)

	if distance > self.ranged_spec.max_range or distance < self.ranged_spec.min_range then
		self:reset_animation()
		self.state = states.FAILED
		debug("RangedAttack failed due to out of range")
		return
	end

	-- Reset animation when counter runs out
	if self.animation_time then
		if self.animation_time < 0 then
			self:reset_animation()
		else
			self.animation_time = self.animation_time - dtime
		end
	end

	-- Start hit when recharged
	self.recharge = self.recharge - dtime
	if self.recharge < 0 then
		self.recharge = self.recharge + self.ranged_spec.tool_capabilities.full_punch_interval

		-- Aim from the archer's head to the target's chest
		local offset_from = vector.add(pos, vector.new(0, 0.7, 0))
		local offset_target = vector.add(target_pos, vector.new(0, 0.5, 0))

		-- It can't shoot at targets it can't see!
		if not conquer.line_of_sight(offset_from, offset_target, { self.object, self.target }) then
			self.state = states.FAILED
			debug("RangedAttack failed due to no line of sight")
			return
		end

		-- Shoot into the target's velocity
		local guessed_time = vector.distance(offset_from, offset_target) * 0.11
		local shootpast = vector.multiply(self.target:get_velocity(), guessed_time)
		offset_target = vector.add(offset_target, shootpast)

		-- Face shoot dir
		local delta = vector.subtract(offset_target, offset_from)
		self.object:set_yaw(math.atan2(delta.z, delta.x) - math.pi / 2)

		-- Shoot!
		local arrow = minetest.add_entity(offset_from, "conquer:arrow")
		if not arrow:get_luaentity():setup(self.ranged_spec.tool_capabilities, offset_target) then
			arrow:remove()
			debug("RangedAttack failed because arrow entity setup failed")
			self.state = states.FAILED
			return
		end

		local animation = self.animations.mine
		self.object:set_animation(animation, 30, 0)
		self.animation_time = (animation.y - animation.x) / 30
	end
end
