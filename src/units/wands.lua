function conquer.register_wand(name, def)
	assert(def.on_node or
			def.on_enemy_unit or def.on_own_unit or
			def.on_enemy_building or def.on_own_building or
			def.on_enemy_target)

	def.range = def.range or 25
	def.groups = def.groups or {}
	def.groups.not_in_creative_inventory = 1

	local function get_unit_alliance(player, obj)
		local entity = obj:get_luaentity()
		local session, country = conquer.get_session_country(player)

		if not entity or not entity.get_unit_type then
			return nil
		elseif entity.session_id ~= session.id then
			minetest.chat_send_player(player:get_player_name(),
				minetest.colorize("#FF4444", "Unit is not in this conquer session!"))
			return nil
		else
			return entity.country_id == country.id
		end
	end

	local function run_actions(actions, player, ...)
		assert(type(actions) == "table")
		assert(player.get_player_name, "run_actions expects a player")

		local units = conquer.get_selected_units(player)
		if #units == 0 then
			minetest.chat_send_player(player:get_player_name(), "Select a unit first!")
			return
		end

		for i=1, #actions do
			for j=1, #units do
				if def[actions[i]] then
					def[actions[i]](player, units[j], ...)
				end
			end
		end
	end

	local function run_action(action_name, player, ...)
		assert(type(action_name) == "string")
		run_actions({ action_name }, player, ...)
	end

	def.on_use = function(_, player, pointed_thing)
		if not player then
			return
		end

		if pointed_thing.type == "object" then
			pointed_thing.ref:punch(player, 1.0, { full_punch_interval=1.0 }, nil)
		elseif pointed_thing.type == "node" then
			local node = minetest.get_node(pointed_thing.under)
			if minetest.get_item_group(node.name, "conquer") == 0 then
				conquer.deselect_all_units(player)
			else
				local node_def = minetest.registered_nodes[node.name]
				if node_def and node_def.on_punch then
					node_def.on_punch(pointed_thing.under, node, player)
				end
			end
		end
		return player:get_wielded_item()
	end

	local function command(stack, player, pointed_thing)
		if pointed_thing.type == "object" then
			local alliance = get_unit_alliance(player, pointed_thing.ref)
			if (def.on_enemy_unit or def.on_enemy_target) and alliance == false then
				run_actions({"on_enemy_unit", "on_enemy_target"}, player, pointed_thing.ref)
			elseif def.on_own_unit and alliance == true then
				run_action("on_own_unit", player, pointed_thing.ref)
			else
				return minetest.item_place(stack, player, pointed_thing)
			end
		elseif pointed_thing.type == "node" then
			local alliance = conquer.get_is_building_owner(player, pointed_thing.under)
			if (def.on_enemy_building or def.on_enemy_target) and alliance == false then
				run_actions({"on_enemy_building", "on_enemy_target"}, player, pointed_thing.under)
			elseif def.on_own_building and alliance == true then
				run_action("on_own_building", player, pointed_thing.under)
			elseif def.on_node then
				run_action("on_node", player, pointed_thing.above)
			else
				return minetest.item_place(stack, player, pointed_thing)
			end
		end
	end

	def.on_secondary_use = command
	def.on_place = command

	minetest.register_craftitem(name, def)
end


local function move_on_node(_, unit, pos)
	local path = conquer.find_path(unit:get_pos(), pos, 20, 1, 1)
	if not path then
		return false
	end

	local unit_type = unit:get_luaentity():get_unit_type()
	local action = conquer.behaviour.FollowPath:new(path, unit_type.animations)
	unit:get_luaentity():set_action(action)
end

conquer.register_wand("conquer:move", {
	description = "Move",
	inventory_image = "conquer_wand_move.png",
	on_node = move_on_node,
})

conquer.register_wand("conquer:melee", {
	description = "Melee",
	inventory_image = "conquer_wand_melee.png",

	on_enemy_target = function(_, unit, target)
		local unit_type = unit:get_luaentity():get_unit_type()
		if not unit_type.melee then
			return
		end

		local move_to = conquer.behaviour.MoveToNearTarget:new(target, unit_type.animations)
		local attack = conquer.behaviour.MeleeAttack:new(target, unit_type.melee, unit_type.animations)
		local action = conquer.behaviour.FickleSequence:new({ move_to, attack })
		unit:get_luaentity():set_action(action)
	end,

	on_node = move_on_node,
})

conquer.register_wand("conquer:ranged", {
	description = "Ranged",
	inventory_image = "conquer_wand_ranged.png",

	on_enemy_unit = function(_, unit, target)
		local unit_type = unit:get_luaentity():get_unit_type()
		if not unit_type.ranged then
			return
		end

		local action = conquer.behaviour.RangedAttack:new(target, unit_type.ranged, unit_type.animations)
		unit:get_luaentity():set_action(action)
	end,

	on_node = move_on_node,
})
