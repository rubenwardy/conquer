local _unit_types = {}
local _selected_units = conquer.make_context_provider()

local function get_selected_units_raw(player)
	assert(player.get_player_name, "get_selected_units expects a player")

	local session, country = conquer.get_session_country(player)
	local context = _selected_units(player)
	if not country or not context.selected_units then
		return {}, false, nil, nil
	end

	local changed = false
	local units = context.selected_units

	local pointer=1
	for i=1, #units do
		if units[i]:get_luaentity() ~= nil then
			units[pointer] = units[i]
			pointer = pointer + 1
		end
	end
	for i=pointer, #units do
		changed = true
		units[i] = nil
	end

	return units, changed, session, country
end

function conquer.get_selected_units(player)
	local units, changed, session, country = get_selected_units_raw(player)

	if changed then
		conquer.run_on_selected_units_changed(player, session, country, units)
	end

	return units
end

function conquer.select_unit(player, obj)
	assert(player.get_player_name, "select_unit expects a player")
	assert(obj.get_pos and obj.get_luaentity)

	local session, country = conquer.get_session_country(player)
	if not session then
		return false
	end

	local entity = obj:get_luaentity()
	if not entity then
		return false
	end

	if not entity.select or not entity:select(session, country) then
		return false
	end

	local context = _selected_units(player)
	context.selected_units = context.selected_units or {}
	table.insert(context.selected_units, obj)

	conquer.run_on_selected_units_changed(player, session, country, get_selected_units_raw(player))

	return true
end

function conquer.select_units(player, objs)
	assert(player.get_player_name, "select_unit expects a player")
	assert(type(objs) == "table")

	local session, country = conquer.get_session_country(player)
	if not session then
		return 0
	end

	local context = _selected_units(player)
	context.selected_units = context.selected_units or {}

	local count = 0
	for i=1, #objs do
		local entity = objs[i]:get_luaentity()
		if entity and entity.select and entity:select(session, country) then
			table.insert(context.selected_units, objs[i])
			count = count + 1
		end
	end

	conquer.run_on_selected_units_changed(player, session, country, get_selected_units_raw(player))

	return count
end

function conquer.deselect_unit(player, unit)
	assert(player.get_player_name, "deselect_unit expects a player")
	assert(unit.get_luaentity, "deselect_unit expects a unit")

	if not unit:get_luaentity() then
		return false
	end

	local session, country = conquer.get_session_country(player)
	if not session then
		return false
	end

	local context = _selected_units(player)
	local units = context.selected_units or {}

	for i=1, #units do
		if units[i] == unit then
			table.remove(units, i)
			unit:get_luaentity():deselect()

			conquer.run_on_selected_units_changed(player, session, country, get_selected_units_raw(player))
			return true
		end
	end

	return false
end

function conquer.deselect_all_units(player)
	assert(player.get_player_name, "deselect_all_units sexpects a player")

	local session, country = conquer.get_session_country(player)
	if not session then
		return false
	end

	local context = _selected_units(player)
	local units = context.selected_units or {}
	for i=#units, 1, -1 do
		if units[i]:get_luaentity() then
			units[i]:get_luaentity():deselect()
		end
		units[i] = nil
	end

	assert(#units == 0 and next(units) == nil)

	conquer.run_on_selected_units_changed(player, session, country, units)

	return true
end

function conquer.replace_select_unit(player, obj)
	assert(player.get_player_name, "deselect_all_units sexpects a player")

	local session, country = conquer.get_session_country(player)
	if not session then
		return false
	end

	local context = _selected_units(player)
	local units = context.selected_units or {}
	context.selected_units = units

	for i=#units, 1, -1 do
		if units[i]:get_luaentity() then
			units[i]:get_luaentity():deselect()
		end
		units[i] = nil
	end

	local entity = obj:get_luaentity()
	if not entity then
		return false
	end

	if not entity.select or not entity:select(session, country) then
		return false
	end

	table.insert(units, obj)
	assert(#units == 1)

	conquer.run_on_selected_units_changed(player, session, country, units)

	return true
end

function conquer.register_unit(name, def)
	assert(not _unit_types[name], "Unit type already registered!")
	assert(def.get_properties, "get_properties is required in unit type")
	assert(def.craft, "def.craft is required, as a table with at least time")
	assert(def.craft.time, "craft time is required")

	assert(not def.melee or def.melee.tool_capabilities)
	assert(not def.ranged or type(def.ranged.min_range) == "number")
	assert(not def.ranged or type(def.ranged.max_range) == "number")
	assert(not def.ranged or def.ranged.max_range > def.ranged.min_range)
	assert(not def.ranged or def.ranged.tool_capabilities)

	def.name = name
	_unit_types[name] = def
end

function conquer.get_unit_type(name)
	assert(type(name) == "string")

	return _unit_types[name]
end

function conquer.get_unit_types()
	return _unit_types
end

function conquer.get_unit_alliance(one, two)
	local entity_one = one:get_luaentity()
	local entity_two = two:get_luaentity()
	if not entity_one or not entity_two or
			not entity_one.get_unit_type or not entity_two.get_unit_type or
			entity_one.session_id ~= entity_two.session_id then
		return nil
	else
		return entity_one.country_id == entity_two.country_id
	end
end
