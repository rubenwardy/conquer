local selections = {}

minetest.register_on_leaveplayer(function(player)
	selections[player:get_player_name()] = nil
end)

local function cancel(_, player, _)
	local pname = player:get_player_name()
	if selections[pname] then
		selections[pname] = nil
		minetest.chat_send_all(minetest.colorize("#808080",
					conquer.S("Cancelled selection.")))
	end
end

minetest.register_craftitem("conquer:box_select", {
	description = "Box Select",
	inventory_image = "conquer_box_select.png",

	range = 25,

	groups = { not_in_creative_inventory = 1 },

	on_place = cancel,
	on_secondary_use = cancel,

	on_use = function(_, player, pointed_thing)
		if not player then
			return
		end

		if pointed_thing.type == "object" then
			pointed_thing.ref:punch(player, 1.0, { full_punch_interval=1.0 }, nil)
			return player:get_wielded_item()
		end

		if pointed_thing.type ~= "node" then
			return player:get_wielded_item()
		end

		local pname = player:get_player_name()
		local selected = selections[pname]
		if selected then
			conquer.deselect_all_units(player)

			local min, max = vector.sort(pointed_thing.under, selected)
			min.y = min.y - 10
			max.y = max.y + 10

			local objs = minetest.get_objects_in_area(min, max)
			local count = conquer.select_units(player, objs)

			minetest.chat_send_all(minetest.colorize("#808080",
					conquer.S("Selected @1 units.", count)))
			selections[pname] = nil
		else
			selections[pname] = vector.new(pointed_thing.under)
			minetest.chat_send_all(minetest.colorize("#808080",
					conquer.S("Now select another position, or right-click to cancel.")))
		end

		return player:get_wielded_item()
	end,
})
