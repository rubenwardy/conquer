local states = {
	RUNNING = "running",
	FAILED = "failed",
	SUCCESS = "success",
}


conquer.behaviour = {
	states = states,
}


local Node = conquer.class()
function Node:set_object(object)
	assert(object.get_pos and object.get_luaentity)
	self.object = object
end
conquer.behaviour.Node = Node


local function debug(...) -- luacheck: ignore
	-- minetest.log("warning", ...)
end


--
-- Behaviour Tree Primitives
--

-- A fickle sequence runs each child every tick
local FickleSequence = conquer.class(Node)
conquer.behaviour.FickleSequence = FickleSequence

function FickleSequence:constructor(children)
	self.children = children
	self.state = states.RUNNING
end

function FickleSequence:set_object(object)
	self.object = object
	for i=1, #self.children do
		self.children[i]:set_object(object)
	end
end

function FickleSequence:get_state()
	return self.state
end

function FickleSequence:on_step(dtime)
	self.state = states.RUNNING

	for i=1, #self.children do
		local child = self.children[i]
		child:on_step(dtime)

		local state = child:get_state()
		if state == states.FAILED then
			self.state = states.FAILED
			return
		elseif state == states.RUNNING then
			return
		end

		assert(state == states.SUCCESS)
	end

	self.state = states.SUCCESS
end


-- A sequence will remember which was the last to run
local Sequence = conquer.class(Node)
conquer.behaviour.Sequence = Sequence

function Sequence:constructor(children)
	self.failed = false
	self.children = children
end

function Sequence:set_object(object)
	self.object = object
	for i=1, #self.children do
		self.children[i]:set_object(object)
	end
end

function Sequence:get_state()
	if self.failed then
		return states.FAILED
	elseif #self.children == 0 then
		return states.SUCCESS
	else
		return states.RUNNING
	end
end

function Sequence:on_step(dtime)
	local child = self.children[1]
	if child then
		child:on_step(dtime)

		local state = child:get_state()
		if state == states.SUCCESS then
			table.remove(self.children, 1)
		elseif state == states.FAILED then
			self.failed = true
			return
		end
	end
end


--
-- Implementations
--


local FollowPath = conquer.class(Node)
conquer.behaviour.FollowPath = FollowPath

local function get_pos_from_node(next)
	local collision_box_height =
			conquer.get_collision_box_height(vector.add(next, vector.new(0, -1, 0)))
	return vector.add(next, vector.new(0, collision_box_height - 0.5, 0))
end

function FollowPath:constructor(path, animations)
	self.animation = nil
	self.animations = animations

	assert(self.animations.stand, "Needs stand animation")
	assert(self.animations.walk, "Needs walk animation")

	self.path = {}
	self.path_i = 1

	-- Skip the first node
	for i=2, #path do
		self.path[i - 1] = vector.add(path[i], vector.new(0, -0.49, 0))
	end

	if #self.path == 0 then
		self.path = nil
	end
end

function FollowPath:get_state()
	return self.path and states.RUNNING or states.SUCCESS
end

function FollowPath:on_step(dtime)
	if not self.path then
		return
	end

	local next = self.path[self.path_i]
	if not next then
		self.object:set_pos(get_pos_from_node(self.path[#self.path]))
		self.object:set_velocity(vector.new())
		self:set_walking(false)
		self.path = nil
		return
	end
	next = get_pos_from_node(next)

	local from = self.object:get_pos()

	local distance = vector.distance(from, next)
	if distance < 0.1 then
		self.path_i = self.path_i + 1
		self:on_step(dtime)
		return
	end

	local step = vector.multiply(vector.normalize(vector.subtract(next, from)), 1)
	self.object:set_velocity(step)
	local target = vector.add(from, vector.multiply(step, dtime))
	self.object:move_to(target, true)

	self.object:set_yaw(math.atan2(step.z, step.x) - math.pi / 2)
	self:set_walking(true)
end

function FollowPath:set_walking(walking)
	if self.animation ~= nil and self.animation == walking then
		return
	end

	self.animation = walking
	self.object:set_animation(walking and self.animations.walk or self.animations.stand, 30, 0)
end


local MoveToNearTarget = conquer.class(Node)
conquer.behaviour.MoveToNearTarget = MoveToNearTarget

function MoveToNearTarget:constructor(target, animations)
	self.target = conquer.create_target(target)
	self.animations = animations
	self.distance = 1

	if target.is_node_target then
		self.distance = self.distance + 0.8
	end

	self.child = nil
	self.last_pos = nil
	self.failed = false
end

function MoveToNearTarget:get_state()
	if self.failed or not self.target or not self.target:get_pos() then
		return states.FAILED
	elseif vector.distance(self.object:get_pos(), self.target:get_pos()) < self.distance then
		return states.SUCCESS
	else
		return states.RUNNING
	end
end

function MoveToNearTarget:on_step(dtime)
	assert(self.target ~= self.object)
	if self:get_state() ~= states.RUNNING then
		if self.child then
			self.child:set_walking(false)
			self.object:set_velocity(vector.new())
		end
		return
	end

	local target_pos = self.target:get_pos()
	if not self.last_pos or
			not vector.equals(vector.round(self.last_pos), vector.round(target_pos)) then
		local path = conquer.find_path_to_target(self.object:get_pos(), self.target)
		if not path then
			debug("MoveToNearTarget failed due to no path")
			self.failed = true
			return
		end

		self.child = conquer.behaviour.FollowPath:new(path, self.animations)
		self.child:set_object(self.object)
		self.last_pos = target_pos
	end

	if self.child then
		self.child:on_step(dtime)
	end
end
