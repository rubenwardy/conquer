function conquer.register_building(name, base_def)
	assert(base_def.damage_levels and #base_def.damage_levels > 0)
	assert(type(base_def.health) == "number")
	assert(type(base_def.resources) == "table")
	assert(base_def.on_place_building)
	assert(not base_def.after_place_node)

	local levels = base_def.damage_levels
	base_def.damage_levels = nil

	base_def.groups.choppy = 3
	base_def.groups.oddly_breakable_by_hand = 1
	base_def.groups.conquer = 1
	base_def.groups.conquer_damagable = 1

	base_def.after_place_node = function(pos, player)
		if not player then
			minetest.set_node(pos, { name = "air" })
			return true
		end

		local session, country = conquer.get_session_country(player)
		if #base_def.resources > 0 and not conquer.has_resources(country, base_def.resources) then
			local resources = conquer.format_resource_list(base_def.resources)
			minetest.chat_send_player(player:get_player_name(),
				conquer.S("Missing required resources. @1 requires: @2", base_def.description, resources))
			minetest.set_node(pos, { name = "air" })
			return true
		end

		if not base_def.on_place_building(pos, player, session, country) then
			minetest.set_node(pos, { name = "air" })
			return true
		end

		assert(conquer.take_resources(country, base_def.resources))
		return true
	end

	base_def.drop = { max_items = 1, items = {} }

	base_def.get_session_country = function(pos)
		local meta = minetest.get_meta(pos)

		local session = conquer.get_session_by_id(meta:get_string("conquer:session"))
		local country = session and session:get_country_by_id(meta:get_string("conquer:country"))
		if not session or not country then
			return nil, nil
		end

		return session, country
	end

	base_def.can_dig = function(pos)
		local session, country = base_def.get_session_country(pos)
		return session == nil and country == nil
	end

	base_def.get_hp = function(self, pos)
		local meta = minetest.get_meta(pos)
		return tonumber(meta:get("conquer:health") or self.health)
	end

	base_def.damage = function(self, pos, time_since_last_punch, tool_capabilities)
		local meta = minetest.get_meta(pos)
		local health = tonumber(meta:get("conquer:health") or self.health)
		if health <= 0 then
			return
		end

		local function limit(x, min, max)
			return math.min(math.max(x, min), max)
		end

		local damage = 0
		for group, base_damage in pairs(tool_capabilities.damage_groups) do
			damage = damage +
				base_damage
				* limit(time_since_last_punch / tool_capabilities.full_punch_interval, 0.0, 1.0)
				* (self.armor_groups[group] or 0) / 100.0
		end

		health = health - damage
		meta:set_string("infotext", conquer.S("Health: @1", health))
		meta:set_int("conquer:health", health)

		if health <= 0 then
			local session = conquer.get_session_by_id(meta:get_string("conquer:session"))
			local country = session:get_country_by_id(meta:get_string("conquer:country"))
			conquer.run_on_building_destroyed(session, country, name, pos)
		end

		if health < self.min_health and self.next_stage then
			minetest.swap_node(pos, { name = self.next_stage })
		end
	end

	base_def.heal = function(self, pos)
		local meta = minetest.get_meta(pos)
		local health = tonumber(meta:get("conquer:health") or self.health)

		local heal_amount = math.min(self.heal_amount or 10, self.health - health)

		health = health + heal_amount
		meta:set_int("conquer:health", health)

		assert(health <= self.health)

		if health > self.max_health and self.prev_stage then
			minetest.swap_node(pos, { name = self.prev_stage })
		end
		return true
	end

	local last_min_health = base_def.health
	local prev_stage = nil
	for i=1, #levels do
		local level = levels[i]
		if i == 1 then
			level.name = ("conquer:%s"):format(name)
		else
			level.name = ("conquer:%s_damaged_%d"):format(name, i)
		end
		level.stage_i = i

		local stage_def = conquer.extend(base_def, level)
		assert(stage_def.stage_i == i)
		assert(stage_def.armor_groups)
		assert(not stage_def.max_health)

		if i == #levels then
			assert(not stage_def.min_health)
			stage_def.min_health = 0
		else
			stage_def.next_stage = ("conquer:%s_damaged_%d"):format(name, i + 1)
			assert(stage_def.min_health)
		end

		stage_def.max_health = last_min_health
		last_min_health = stage_def.min_health
		stage_def.prev_stage = prev_stage
		prev_stage = stage_def.name

		minetest.register_node(":" .. level.name, stage_def)
	end
end

function conquer.get_building_session_country(pos)
	local node = minetest.get_node(pos)
	local node_def = minetest.registered_nodes[node and node.name]
	if not (node_def and node_def.get_session_country) then
		return nil
	end

	return node_def.get_session_country(pos)
end

function conquer.get_is_building_owner(player, pos)
	local node = minetest.get_node(pos)
	local node_def = minetest.registered_nodes[node and node.name]
	if not (node_def and node_def.get_session_country) then
		return nil
	end

	local session, country = conquer.get_session_country(player)
	if not session then
		return nil
	end

	local session2, country2 = node_def.get_session_country(pos)
	if not session2 or session2.id ~= session.id then
		return nil
	else
		return country2.id == country.id
	end
end

function conquer.get_building_hp(pos)
	local def = minetest.registered_nodes[minetest.get_node(pos).name]
	if not def or not def.get_hp then
		return 0
	end

	return def:get_hp(pos)
end

function conquer.damage_building(pos, time_since_last_punch, tool_capabilities)
	local def = minetest.registered_nodes[minetest.get_node(pos).name]
	if not def or not def.damage then
		return
	end

	def:damage(pos, time_since_last_punch, tool_capabilities)
end

function conquer.heal_building(pos)
	local def = minetest.registered_nodes[minetest.get_node(pos).name]
	if not def or not def.damage then
		return false
	end

	local session, country = def.get_session_country(pos)
	if not session or not country then
		return false
	end

	local enemies = conquer.find_enemy_units(pos, session.id, country.id, 0, 5)
	if #enemies == 0 then
		return def:heal(pos)
	else
		return false
	end
end
