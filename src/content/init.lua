local dir = minetest.get_modpath("conquer") .. "/src/content"
dofile(dir .. "/resources.lua")
dofile(dir .. "/units.lua")
dofile(dir .. "/keep.lua")
dofile(dir .. "/barracks.lua")
dofile(dir .. "/farm.lua")
