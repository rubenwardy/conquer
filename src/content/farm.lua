local S = conquer.S

conquer.register_building("farm", {
	description = S("Farm"),

	groups = { conquer = 1, not_in_creative_inventory = 1 },

	drawtype = "mesh",
	tiles = {
		"conquer_farm.png",
		"conquer_farm_blades.png",
	},
	mesh = "conquer_farm.obj",

	paramtype = "light",
	sunlight_propagates = true,

	resources = { "conquer:wood 20" },

	health = 50,
	armor_groups = {
		conquer_blunt = 100,
		conquer_sharp = 80,
	},
	damage_levels = {
		{},
	},

	selection_box = {
		type = "fixed",
		fixed = {
			{ -5/16, -8/16, -5/16, 5/16, -1/16, 5/16 },
		},
	},

	collision_box = {
		type = "fixed",
		fixed = {
			{ -5/16, -8/16, -5/16, 5/16, -1/16, 5/16 },
		},
	},

	on_place_building = function(pos, player, session, country)
		local success, message = conquer.place_building("farm", session, country, pos)
		if not success then
			minetest.chat_send_player(player:get_player_name(), message)
			return false
		end

		local meta = minetest.get_meta(pos)
		meta:set_string("conquer:session", session.id)
		meta:set_string("conquer:country", country.id)

		local timer = minetest.get_node_timer(pos)
		timer:start(10)

		return true
	end,

	on_timer = function(pos)
		conquer.heal_building(pos)

		local hp = conquer.get_building_hp(pos)
		if hp < 40 then
			return true
		end

		local session, country = conquer.get_building_session_country(pos)
		if not session then
			return false
		end

		conquer.add_resource(country, "conquer:food")

		return true
	end,
})
