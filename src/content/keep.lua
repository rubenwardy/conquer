local S = conquer.S

local function on_interact(pos, _, player)
	local session, country = conquer.get_session_country(player)
	if not session or not country then
		return
	end

	local meta = minetest.get_meta(pos)
	if session.id ~= meta:get_string("conquer:session") or
			country.id ~= meta:get_string("conquer:country") then
		minetest.chat_send_player(player:get_player_name(),
			"This is not your keep")
		return
	end

	conquer.show_keep_gui(player, session, country)
end

conquer.register_building("keep", {
	description = S("Keep"),

	groups = { conquer = 1, not_in_creative_inventory = 1 },

	drawtype = "mesh",
	mesh = "conquer_keep.obj",

	resources = {},

	health = 100,
	armor_groups = {
		conquer_blunt = 20,
		conquer_sharp = 0,
	},
	damage_levels = {
		{
			min_health = 80,
			tiles = {
				"conquer_keep.png"
			},
		},

		{
			min_health = 50,
			tiles = {
				"conquer_keep_damaged.png"
			},
		},

		{
			-- min_health = 10,
			tiles = {
				"conquer_keep_damaged.png"
			},
			mesh = "conquer_keep_damaged.obj",
		},

		-- {
		-- 	tiles = {
		-- 		"conquer_keep_damaged.png"
		-- 	},
		-- 	mesh = "conquer_keep_destroyed.obj",
		-- },
	},

	selection_box = {
		type = "fixed",
		fixed = {
			{ -6/16, -8/16, -6/16, 6/16, 6/16, 6/16 },
		},
	},

	collision_box = {
		type = "fixed",
		fixed = {
			{ -6/16, -8/16, -6/16, 6/16, 4/16, 6/16 },
		},
	},

	on_place_building = function(pos, player, session, country)
		local success, message = conquer.place_keep(session, country, pos)
		if not success then
			minetest.chat_send_player(player:get_player_name(), message)
			minetest.set_node(pos, { name = "air" })
			return false
		end

		local meta = minetest.get_meta(pos)
		meta:set_string("conquer:session", session.id)
		meta:set_string("conquer:country", country.id)

		local inv = meta:get_inventory()
		inv:set_size("stockpile", 4)
		inv:add_item("stockpile", ItemStack("conquer:food 30"))
		inv:add_item("stockpile", ItemStack("conquer:wood 40"))
		inv:add_item("stockpile", ItemStack("conquer:stone 60"))

		return true
	end,

	on_punch = on_interact,
	on_rightclick = on_interact,

	allow_metadata_inventory_move = function()
		return 0
	end,

	allow_metadata_inventory_put = function()
		return 0
	end,

	allow_metadata_inventory_take = function()
		return 0
	end,
})

conquer.register_on_building_placed(function(session, country, building_type)
	if building_type ~= "keep" then
		return
	end

	local spawn_pos = conquer.find_empty_neighbour(country.keep) or
			vector.add(country.keep, vector.new(0, 1, 0))
	spawn_pos = vector.add(spawn_pos, vector.new(0, -0.49, 0))
	local obj = minetest.add_entity(spawn_pos, "conquer:unit")
	obj:get_luaentity():setup("knight", session, country)
end)

function conquer.add_resource(country, stack)
	if not country.keep then
		return false
	end

	local inv = minetest.get_inventory({ type = "node", pos = country.keep })
	return inv:add_item("stockpile", stack)
end

function conquer.has_resources(country, stacks)
	if not country.keep then
		return false
	end

	local inv = minetest.get_inventory({ type = "node", pos = country.keep })
	for _, stack in pairs(stacks) do
		if not inv:contains_item("stockpile", stack) then
			return false
		end
	end

	return true
end

function conquer.take_resources(country, stacks)
	if not country.keep or not conquer.has_resources(country, stacks) then
		return false
	end

	local inv = minetest.get_inventory({ type = "node", pos = country.keep })
	for _, stack in pairs(stacks) do
		local items = inv:remove_item("stockpile", stack)
		assert(items:get_count() == ItemStack(stack):get_count())
	end

	return true
end
