minetest.register_craftitem("conquer:food", {
	description = conquer.S("Food"),
	inventory_image = "farming_bread.png",
	stack_max = 1000,
	groups = { conquer_resource = 1, not_in_creative_inventory = 1 },
})

minetest.register_craftitem("conquer:wood", {
	description = conquer.S("Wood"),
	inventory_image = minetest.inventorycube("default_wood.png"),
	stack_max = 1000,
	groups = { conquer_resource = 1, not_in_creative_inventory = 1 },
})

minetest.register_craftitem("conquer:stone", {
	description = conquer.S("Stone"),
	inventory_image = minetest.inventorycube("default_stone.png"),
	stack_max = 1000,
	groups = { conquer_resource = 1, not_in_creative_inventory = 1 },
})
