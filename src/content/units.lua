local function register_playerlike_unit(name, def)
	local defaults = {
		max_hp = 30,

		animations = {
			stand     = {x = 0,   y = 79},
			lay       = {x = 162, y = 166},
			walk      = {x = 168, y = 187},
			mine      = {x = 189, y = 198},
			walk_mine = {x = 200, y = 219},
			sit       = {x = 81,  y = 160},
		},

		get_properties = function(_, country)
			return {
				mesh = "conquer_humanlike.b3d",
				textures = { ("conquer_unit_%s_%s.png"):format(name, country.colors.skin) },
				visual = "mesh",
				visual_size = {x = 0.5, y = 0.5},
				collisionbox = {-0.15, 0.0, -0.15, 0.15, 0.85, 0.15},
				stepheight = 1,
				physical = true,
				collide_with_objects = false,
			}
		end,
	}

	conquer.register_unit(name, conquer.extend(defaults, def))
end

register_playerlike_unit("knight", {
	description = conquer.S("Knight"),
	icon = "conquer_unit_knight_icon.png",

	craft = {
		time = 10,
		resources = {
			"conquer:food 12",
		},
	},

	armor_groups = { conquer_blunt = 60, conquer_sharp = 90 },
	melee = {
		tool_capabilities = {
			full_punch_interval = 0.8,
			max_drop_level=1,
			groupcaps={
				conquer_blunt={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=30, maxlevel=2},
			},
			damage_groups = { conquer_blunt = 5 },
		},
	},
})

register_playerlike_unit("archer", {
	description = conquer.S("Archer"),
	icon = "conquer_unit_archer_icon.png",

	craft = {
		time = 6,
		resources = {
			"conquer:food 8",
		},
	},

	armor_groups = { conquer_blunt = 100, conquer_sharp = 100 },
	melee = {
		tool_capabilities = {
			full_punch_interval = 0.6,
			max_drop_level=2,
			groupcaps={
				conquer_blunt={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=300, maxlevel=3},
			},
			damage_groups = { conquer_blunt = 1 },
		},
	},
	ranged = {
		min_range = 1.8,
		max_range = 10,
		tool_capabilities = {
			full_punch_interval = 2,
			max_drop_level=1,
			groupcaps={
				conquer_sharp={times={[1]=2.5, [2]=1.20, [3]=0.35}, uses=30, maxlevel=2},
			},
			damage_groups = { conquer_sharp = 3 },
		},
	},
})
