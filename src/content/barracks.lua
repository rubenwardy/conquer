local S = conquer.S

conquer.register_building("barracks", {
	description = S("Barracks"),

	groups = { conquer = 1, not_in_creative_inventory = 1 },

	drawtype = "mesh",
	tiles = {
		"conquer_barracks.png"
	},
	mesh = "conquer_barracks.obj",

	paramtype = "light",
	sunlight_propagates = true,

	resources = { "conquer:stone 30" },

	health = 100,
	armor_groups = {
		conquer_blunt = 100,
		conquer_sharp = 20,
	},
	damage_levels = {
		{
			min_health = 80,
			tiles = {
				"conquer_barracks.png"
			},
		},

		{
			min_health = 50,
			tiles = {
				"conquer_barracks_damaged.png"
			},
		},

		{
			min_health = 10,
			tiles = {
				"conquer_barracks_damaged.png"
			},
			mesh = "conquer_barracks_damaged.obj",
		},

		{
			tiles = {
				"conquer_barracks_damaged.png"
			},
			mesh = "conquer_barracks_destroyed.obj",
		},
	},

	selection_box = {
		type = "fixed",
		fixed = {
			{ -5/16, -8/16, -5/16, 5/16, -1/16, 5/16 },
		},
	},

	collision_box = {
		type = "fixed",
		fixed = {
			{ -5/16, -8/16, -5/16, 5/16, -1/16, 5/16 },
		},
	},

	on_place_building = function(pos, player, session, country)
		local success, message = conquer.place_building("barracks", session, country, pos)
		if not success then
			minetest.chat_send_player(player:get_player_name(), message)
			return false
		end

		local meta = minetest.get_meta(pos)
		meta:set_string("conquer:session", session.id)
		meta:set_string("conquer:country", country.id)

		return true
	end,

	on_timer = function(pos)
		local meta = minetest.get_meta(pos)

		local session = conquer.get_session_by_id(meta:get_string("conquer:session"))
		if not session then
			return false
		end

		local country = session:get_country_by_id(meta:get_string("conquer:country"))
		if not country then
			return false
		end

		local unit_type_name = meta:get("conquer:unit_type")
		if not unit_type_name then
			return false
		end

		meta:set_string("conquer:unit_type", "")

		local spawn_pos = conquer.find_empty_neighbour(country.barracks) or
			vector.add(country.barracks, vector.new(0, 1, 0))
		spawn_pos = vector.add(spawn_pos, vector.new(0, -0.49, 0))
		local obj = minetest.add_entity(spawn_pos, "conquer:unit")
		obj:get_luaentity():setup(unit_type_name, session, country)

		return false
	end,
})

local function get_stockpile_by_country(country)
	if not country.keep then
		return nil
	end

	local inv = minetest.get_inventory({
		type = "node",
		pos = country.keep
	})
	if not inv then
		return nil
	end

	return inv
end

conquer.register_on_can_train_unit(function(_, country, unit_type)
	if not country.barracks then
		return S("You need a barracks")
	end

	if conquer.get_building_hp(country.barracks) < 50 then
		return S("Unable to train whilst the barracks are damaged")
	end

	local inv = get_stockpile_by_country(country)
	if not inv then
		return S("Unable to find stockpile")
	end

	local meta = minetest.get_meta(country.barracks)
	if meta:contains("conquer:unit_type") then
		return S("A unit is already training")
	end

	local craft = unit_type.craft
	for _, item in pairs(craft.resources or {}) do
		if not inv:contains_item("stockpile", item) then
			return S("Missing item @1" , ItemStack(item):get_description())
		end
	end
end)

conquer.register_on_train_unit(function(_, country, unit_type)
	assert(country.barracks)

	local inv = get_stockpile_by_country(country)
	assert(inv)

	local craft = unit_type.craft
	for _, item in pairs(craft.resources or {}) do
		assert(inv:contains_item("stockpile", item))
		inv:remove_item("stockpile", item)
	end

	assert(country.barracks)

	local meta = minetest.get_meta(country.barracks)
	assert(not meta:contains("conquer:unit_type"))

	meta:set_string("conquer:unit_type", unit_type.name)

	local timer = minetest.get_node_timer(country.barracks)
	timer:start(unit_type.craft.time)
end)
