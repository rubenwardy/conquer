conquer = {}

conquer.S = minetest.get_translator("conquer")

assert(minetest.features.pathfinder_works, "This mod requires Minetest 5.4 or later")
assert(minetest.get_objects_in_area, "This mod requires Minetest 5.4 or later")

dofile(minetest.get_modpath("conquer") .. "/src/init.lua")


local storage = minetest.get_mod_storage()

local function save()
	storage:set_int("version", 1)

	local sessions = {}
	for _, session in pairs(conquer.get_sessions()) do
		sessions[session.id] = session:to_table()
	end
	storage:set_string("sessions", minetest.serialize(sessions))
end

local function load()
	if not storage:contains("version") then
		return
	end

	assert(storage:get_int("version") <= 1, "Unsupported conquer serialisation version")

	local sessions = minetest.deserialize(storage:get_string("sessions"))
	for _, session in pairs(sessions) do
		conquer.import_session(session)
	end
end

conquer.set_interval(10, function()
	save()
end)

minetest.register_on_shutdown(save)

load()
