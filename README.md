# Conquer

[![pipeline status](https://gitlab.com/rubenwardy/conquer/badges/master/pipeline.svg)](https://gitlab.com/rubenwardy/conquer/commits/master)

Command your troops to victory in this mini-game.


## License and Credits

Source: LGPLv2.1, Media: CC-BY-SA 3.0.

* conquer_humanlike.b3d from [Player API](https://github.com/minetest/minetest_game/tree/master/mods/player_api) by various contributors, CC BY-SA 3.0.
* conquer_unit_* skins created by GreenXenith, CC-BY-SA 3.0, modified by rubenwardy.
* conquer_arrow_*.png from [Simple Shooter](https://github.com/stujones11/shooter) by Stuart Jones, CC0 1.0.
* conquer_arrow.b3d from [Simple Shooter](https://github.com/stujones11/shooter) by Stuart Jones, CC-BY-SA 3.0.
* conquer_arrow_head.png from MTG, CC-BY-SA 3.0.
* health_*.png from [Gauges](https://content.minetest.net/packages/Calinou/gauges/) by Calinou, CC0.
* All other textures and models created by rubenwardy, CC-BY-SA 3.0.


## Usage

### Overview

Conquer is a mod that adds RTS gameplay. It allows players to start Conquer sub-games, where
they can place buildings, train units, and fight other players.

Conquer is designed with the intention to eventually convert it into a game, with custom mapgen and richer features.
This standalone mini-game version will remain, however.

### Starting or joining a Conquer session

Start a new conquer game by running:

	/conquer start

You'll switch into Conquer playing mode, where you will be given buildings that you can place.

You'll need to place a keep, which you must protect at all costs.

Other players can join your conquer session if they are within 50 nodes by
typing:

	/conquer join yourname

You may leave a game and return to normal playing mode at any time by typing:

	/conquer leave

### Conquer GUI

The Conquer GUI is the central place for monitoring your kingdom.
Once in a session, you can view it by pressing the inventory key (I),
or by punching/right-clicking the keep node.

### Recruiting Units

You'll need to place a barracks to be able to train new units.
Once you have done that, open the Conquer GUI and select a unit to train.

Training a unit requires time and resources.
The barracks will start training the unit as soon as you select it, and with spawn the unit nearby when complete.

Currently, there is no way to obtain resources. This is on the to-do list.

### Controlling Units

Select a unit by left clicking.
You can select multiple units using shift+click.
You can deselect units by pressing Q or clicking somewhere else.

Right-click to command the unit to do something.

Change the active wand by scrolling or pressing a number - just like with normal hotbars:

* **Move:** right-click on a node to move the unit there.
* **Melee Attack:** right-click on a unit or building to move the unit there, and attack.
* **Ranged Attack:** right-click on a unit to fire arrows. The unit must be in
  sight, and must be less than 10 nodes and more than 2 nodes away.

Attacking buildings will damage them. Damaged buildings will be repaired
automatically when there are no enemies nearby.

### Winning

You win the game by being the last surviving country. To defeat another kingdom, you will need to destroy their keep.

You can destroy an enemy's barracks to prevent them from training troops.

### Chat Commands

Anyone:

* `help`: show help.
* `list`: show list of sessions.
* `start`: start session
* `leave`: leave session.
* `join <username>`: join session by username.

Requires `conquer_mgr` priv:

* `new_country`: create new country, do not join it.
* `join_country <name>`: join country.
* `new_join`: create and join country.
* `place`: create and join country, then place keep and barracks where you stand.
* `delete_all`: delete all sessions.

Requires `give`:

* `give <name> <item>`: gives `item` to country `name`.


## Features

* Damageable buildings, with visual indication of health.
* Armour: Knights have armour, which will absorb damage.
* Actions: units can be commanded to move, or perform melee or ranged attack.
* Reactions: archers will automatically target nearby enemies. All units will defend themselves from melee attacks.
* Archers know how to aim to counteract gravity. They also aim where a moving unit will be, rather than where they are currently.
* Translation support.
