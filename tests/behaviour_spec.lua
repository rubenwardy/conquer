local function dofile(path)
	local str = debug.getinfo(2, "S").source:sub(2)
	str = str:match("(.*/)")
	return _G.dofile(str .. "../" .. path)
end

_G.conquer = {}

dofile("src/utils.lua")

local mocks = dofile("tests/mocks.lua")
dofile("src/units/behaviour.lua")

local MockBehaviour = mocks.MockBehaviour

describe("FickleSequence", function()
	local FickleSequence = conquer.behaviour.FickleSequence
	assert(FickleSequence)

	it("runs in order", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = FickleSequence:new({a, b})
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		a.state = "success"

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.state = "success"

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("success", seq:get_state())
	end)

	it("handles failed", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()
		a.state = "success"

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = FickleSequence:new({a, b})
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.state = "failed"

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_true(b.ran)
		assert.equals("failed", seq:get_state())
	end)
end)


describe("Sequence", function()
	local Sequence = conquer.behaviour.Sequence
	assert(Sequence)

	it("runs in order", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = Sequence:new({a, b})
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		a.state = "success"

		reset()
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:on_step(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.state = "success"

		reset()
		seq:on_step(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("success", seq:get_state())
	end)


	it("handles failed", function()
		local a = MockBehaviour:new()
		local b = MockBehaviour:new()
		a.state = "success"

		local function reset()
			a.ran = false
			b.ran = false
		end

		local seq = Sequence:new({a, b})
		assert.equals("running", seq:get_state())

		assert.is_false(a.ran)
		assert.is_false(b.ran)
		seq:on_step(0.1)
		assert.is_true(a.ran)
		assert.is_false(b.ran)
		assert.equals("running", seq:get_state())

		reset()
		seq:on_step(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("running", seq:get_state())

		b.state = "failed"

		reset()
		seq:on_step(0.1)
		assert.is_false(a.ran)
		assert.is_true(b.ran)
		assert.equals("failed", seq:get_state())
	end)
end)
