local function dofile(path)
	local str = debug.getinfo(2, "S").source:sub(2)
	str = str:match("(.*/)")
	return _G.dofile(str .. "../" .. path)
end

local function reset()
	_G.conquer = {}

	dofile("src/utils.lua")
	local mocks = dofile("tests/mocks.lua")
	dofile("src/Session.lua")

	local context = {}
	conquer.make_context_provider = function()
		return function()
			return context
		end
	end

	dofile("src/api.lua")
	dofile("src/units/api.lua")

	mocks.context = context
	return mocks
end

describe("api", function()
	it("create sessions", function()
		reset()

		local pos = vector.new(1,2,3)
		local session = conquer.create_session("player1", pos)
		assert.equals("player1", session.owner)
		assert.same(pos, session.pos)
	end)

	it("place keeps", function()
		reset()

		local session = conquer.Session:new("player1", vector.new(1,2,1))
		local country = { colors = { primary = "red" } }
		local pos = vector.new(1,2,3)

		local callback_called = false
		conquer.register_on_building_placed(function(session2, country2, building_type)
			assert.equals(session, session2)
			assert.equals(country, country2)
			assert.equals("keep", building_type)
			assert.same(pos, country.keep)
			callback_called = true
		end)

		assert.is_true(conquer.place_keep(session, country, pos))
		assert.same(pos, country.keep)
		assert.is_true(callback_called)

		callback_called = false
		assert.is_false(conquer.place_keep(session, country, pos))
		assert.is_false(callback_called)
	end)

	it("place barracks", function()
		reset()

		local session = conquer.Session:new("player1", vector.new(1,2,1))
		local country = { colors = { primary = "red" } }
		local pos = vector.new(1,2,3)

		local callback_called = false
		conquer.register_on_building_placed(function(session2, country2, building_type)
			assert.equals(session, session2)
			assert.equals(country, country2)
			assert.equals("barracks", building_type)
			assert.same(pos, country.barracks)
			assert.is_not_nil(country.keep)
			callback_called = true
		end)

		assert.is_false(conquer.place_building("barracks", session, country, pos))
		assert.is_nil(country.keep)
		assert.is_false(callback_called)

		country.keep = vector.new(3, 2, 3)

		assert.is_true(conquer.place_building("barracks", session, country, pos))
		assert.is_not_nil(country.keep)
		assert.same(pos, country.barracks)
		assert.is_true(callback_called)

		callback_called = false
		assert.is_false(conquer.place_building("barracks", session, country, pos))
		assert.is_not_nil(country.keep)
		assert.is_false(callback_called)
	end)

	it("get selected units", function()
		local mocks = reset()
		local player = mocks.make_player("player1")

		assert.equals(0, #conquer.get_selected_units(player))

		conquer.get_session_country = function()
			return {}, {}
		end

		assert.equals(0, #conquer.get_selected_units(player))

		local a = mocks.make_unit_entity()
		local b = mocks.make_unit_entity()

		do
			mocks.context.selected_units = {
				a,
				mocks.make_expired_object(),
				b,
			}

			local units = conquer.get_selected_units(player)
			assert.equals(2, #units)
			assert.equals(mocks.context.selected_units, units)
			assert.equals(a, units[1])
			assert.equals(b, units[2])

			units = conquer.get_selected_units(player)
			assert.equals(2, #units)
			assert.equals(mocks.context.selected_units, units)
			assert.equals(a, units[1])
			assert.equals(b, units[2])
		end
	end)
end)
