local mocks = {}

_G.minetest = {
	pos_to_string = function(pos)
		return ("%d,%d,%d"):format(pos.x, pos.y, pos.z)
	end,

	get_gametime = function()
		return 5.2
	end,
}

_G.vector = {
	new = function(x, y, z)
		if x and y and z then
			return {x=x, y=y, z=z}
		elseif not x and not y and not z then
			return {x=0, y=0, z=0}
		elseif x and x.x and x.y and x.z then
			return {x=x.x, y=x.y, z=x.z}
		else
			error("Unknown arguments")
		end
	end,
	add = function(a, b)
		return {
			x = a.x + b.x,
			y = a.y + b.y,
			z = a.z + b.z,
		}
	end,
	subtract = function(a, b)
		return {
			x = a.x - b.x,
			y = a.y - b.y,
			z = a.z - b.z,
		}
	end,
	multiply = function(a, s)
		return {
			x = a.x * s,
			y = a.y * s,
			z = a.z * s,
		}
	end,
	divide = function(a, s)
		return {
			x = a.x / s,
			y = a.y / s,
			z = a.z / s,
		}
	end,
	floor = function(vec)
		return {
			x = math.floor(vec.x),
			y = math.floor(vec.y),
			z = math.floor(vec.z),
		}
	end,
	length = function(delta)
		return math.sqrt(delta.x*delta.x + delta.y*delta.y + delta.z*delta.z)
	end,
	distance = function(one, two)
		return vector.length({
			x = one.x - two.x,
			y = one.y - two.y,
			z = one.z - two.z,
		})
	end,
	normalize = function(vec)
		return vector.divide(vec, vector.length(vec))
	end,
}

_G.table.copy = function(tab)
	local ret = {}
	for key, value in pairs(tab) do
		ret[key] = value
	end
	return ret
end



local MockBehaviour = conquer.class()
mocks.MockBehaviour = MockBehaviour

function MockBehaviour:constructor()
	self.state = "running"
	self.ran = false
end

function MockBehaviour:get_state()
	return self.state
end

function MockBehaviour:on_step()
	self.ran = true
end


_G.conquer.S = function(str) return str end


function mocks.make_meta()
	local values = {}

	local function set(_, key, value)
		values[key] = tostring(value)
	end

	return {
		set = set,
		set_string = set,
		set_int = set,
		set_float = set,

		get = function(_, key)
			return values[key]
		end,

		get_string = function(_, key)
			return values[key] or ""
		end,

		get_int = function(_, key)
			return math.floor(tonumber(values[key]))
		end,

		get_float = function(_, key)
			return tonumber(values[key])
		end,
	}
end


local function make_object()
	local pos = vector.new()
	local object = {}

	function object.get_pos()
		return pos
	end

	function object:set_pos(v)
		self.pos = v
	end

	return object
end


function mocks.make_player(name)
	local meta = mocks.make_meta()
	local player = make_object()

	function player.get_player_name()
		return name
	end

	function player.get_meta()
		return meta
	end

	return player
end


function mocks.make_unit_entity(name)
	local meta = mocks.make_meta()
	local player = make_object()

	function player.get_player_name()
		return name
	end

	function player.get_meta()
		return meta
	end

	function player.get_luaentity()
		return {}
	end

	return player
end


function mocks.make_expired_object()
	local object = {}
	setmetatable(object, {
		__index = function(_, key)
			if key:sub(1, 4) == "get_" then
				return function()
					return nil
				end
			else
				return nil
			end
		end
	})
	return object
end


return mocks
