local function dofile(path)
	local str = debug.getinfo(2, "S").source:sub(2)
	str = str:match("(.*/)")
	return _G.dofile(str .. "../" .. path)
end

_G.conquer = {}

dofile("src/utils.lua")
dofile("tests/mocks.lua")

describe("calculate_projectile_direction", function()

	local function check_answer(direction, from, to, initial_speed)
		local s = vector.subtract(to, from)
		s.x = math.sqrt(s.x*s.x + s.z*s.z)

		local u = vector.multiply(direction, initial_speed)
		u.x = math.sqrt(u.x*u.x + u.z*u.z)

		local a = vector.new(0, -8, 0)

		-- Constant horizontal velocity
		local t = s.x / u.x
		assert.is_true(t > 0)

		local s_2 = u.y*t + 0.5*a.y*t*t
		assert.is_true(math.abs(s.y - s_2) < 0.01)
	end

	it("hits axis-bound target", function()
		local from = vector.new(1, 0, 0)
		local to = vector.new(10, 0, 0)
		local initial_speed = 10
		local direction =
				conquer.calculate_projectile_direction(from, to, initial_speed, 8)
		assert.is_not_nil(direction)
		check_answer(direction, from, to, initial_speed)
	end)

	it("hits parallel-bound target", function()
		local from = vector.new(1, 0, 0)
		local to = vector.new(5, 0, 5)
		local initial_speed = 20
		local direction =
				conquer.calculate_projectile_direction(from, to, initial_speed, 8)
		assert.is_not_nil(direction)
		check_answer(direction, from, to, initial_speed)
	end)

	it("hits higher target", function()
		local from = vector.new(1, 0, 0)
		local to = vector.new(5, 3, 5)
		local initial_speed = 20
		local direction =
				conquer.calculate_projectile_direction(from, to, initial_speed, 8)
		assert.is_not_nil(direction)
		check_answer(direction, from, to, initial_speed)
	end)

	it("hits lower target", function()
		local from = vector.new(1, 0, 0)
		local to = vector.new(5, -3, 5)
		local initial_speed = 20
		local direction =
				conquer.calculate_projectile_direction(from, to, initial_speed, 8)
		assert.is_not_nil(direction)
		check_answer(direction, from, to, initial_speed)
	end)

	it("too far", function()
		local from = vector.new(1, 0, 0)
		local to = vector.new(1000, -3, 5)
		local initial_speed = 20
		local direction =
				conquer.calculate_projectile_direction(from, to, initial_speed, 8)

		assert.is_nil(direction)
	end)
end)
