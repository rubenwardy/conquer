local function dofile(path)
	local str = debug.getinfo(2, "S").source:sub(2)
	str = str:match("(.*/)")
	return _G.dofile(str .. "../" .. path)
end

_G.conquer = {}

dofile("src/utils.lua")
dofile("tests/mocks.lua")
dofile("src/Session.lua")

local Session = conquer.Session

describe("session", function()
	it("constructs sessions", function()
		local pos = vector.new(12, 3, 4)
		local session = Session:new("player1", pos)
		assert.equals("player1", session.owner)
		assert.same(pos, session.pos)
	end)

	it("can create countries", function()
		local session = Session:new("player1", vector.new(12, 3, 4))

		local country = session:create_country("player1")
		assert.equals(country.colors.primary, "red")
		assert.equals(1, #session:get_countries())
		assert.equals(country, session:get_countries()[1])
	end)
end)
