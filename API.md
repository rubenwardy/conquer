# API

## Functions

### Sessions

* `conquer.create_session(owner, pos) --> Session`
* `conquer.delete_session(session)`
* `conquer.import_session(data) --> Session`
  * `data` is output from `Session:to_table()`
* `conquer.get_players_in_session(session) --> table of player ObjectRefs`
* `conquer.get_session(player) --> Session`
* `conquer.get_session_country(player) --> Session, Country`
* `conquer.get_session_by_id(pos) --> Session`
* `conquer.get_sessions() --> table of Session`

### Units

* `conquer.get_selected_units(player) -> table of ObjectRefs`
* `conquer.select_unit(player, object_ref) --> boolean`
* `conquer.select_units(player, list_of_object_refs) --> int`
  * Returns number of units selected.
* `conquer.deselect_unit(player, object_ref) --> boolean`
* `conquer.deselect_all_units(player) --> boolean`
* `conquer.get_unit_type(name) --> Unit type def`
* `conquer.get_unit_types() --> table of name to Unit type def`
* `conquer.get_unit_alliance(one, two) --> bool or nil`
  * `one` and `two` are ObjectRefs.
  * True if same country, false otherwise.
  * Returns nil if the `one` and `two` aren't units in the same session.
* `conquer.register_unit(name, def)`. def:
  * `get_properties = function(session, country) --> table`
* `conquer.register_wand(name, def)`. def:
  * Can contain anything in an item definition.
  * `on_node = function(player, unit, pos)`
  * `on_enemy_target = function(player, unit, target)`
    * target is either an ObjectRef or a node position.
  * `on_enemy_unit = function(player, unit, enemy_unit)`
  * `on_own_unit = function(player, unit, own_unit)`
  * `on_enemy_building = function(player, unit, pos)`
  * `on_own_building = function(player, unit, pos)`
* `conquer.can_train_unit(session, country, unit_type) --> boolean, message`
* `conquer.train_unit(session, country, unit_type)`
  * Takes the resources and starts to train a unit.
* `conquer.find_enemy_units(pos, session_id, country_id, min_range, max_range) -> table of ObjectRef`
  * Sorted from closest to furthest.
* `conquer.register_command(name, def)`
  * Registers a subcommand of `/conquer`.
  * `def` is exactly the same as a chat command def.
    * Only `func` and `params` are supported currently.

### Buildings

* `conquer.get_building_session_country(pos) --> session, country`
* `conquer.get_is_building_owner(player, pos)`
  * returns nil if not a building in the player's session,
    false if enemy and true own building.
* `conquer.get_building_hp(pos) --> number`
* `conquer.damage_building(pos, time_since_last_punch, tool_capabilities)`
* `conquer.heal_building(pos) --> boolean`
* `conquer.place_keep(session, country, pos) --> boolean`
    * Doesn't `set_node`. Probably worth using `minetest.item_place_node` instead.
* `conquer.place_building(building_type_name, session, country, pos) --> boolean`
    * Doesn't `set_node`. Probably worth using `minetest.item_place_node` instead.
* `conquer.register_building(name, def)`
  * Anything in a node def, except:
    * `after_place_node`
  * `on_place_building = function(pos, player, session, country)` - required.
    * Return true to allow place, false to cancel.
  * `resources` - required, table of item stacks.
  * `health` - required, number. Typical: 100.
  * `armor_groups` - optional.
  * `damage_levels` - required, table. This is used to override visuals depending on health. Each element is a damage level:
    * `min_health` - the smallest health this level applies to. Required for all but the last level.
    * `armor_groups` - required, optional if defined on building.
    * Any other properties will override building properties

Building definition example:

```lua
{
	-- Nearly any node definition property
	mesh = "conquer_mybuilding.obj",

	-- Required resources
	resources = { "conquer:wood 99" },

	-- Health and armor groups
	health = 100,
	armor_groups = {
		conquer_blunt = 20,
		conquer_sharp = 0,
	},

	-- Override appearance based on health
	damage_levels = {
		{
			min_health = 50,
			tiles = {
				"conquer_mybuilding.png"
			},
		},

		{
			-- min_health = 10,
			tiles = {
				"conquer_mybuilding.png"
			},
			mesh = "conquer_mybuilding_damaged.obj",
		},
	},

	-- Place callback
	on_place_building = function(pos, player, session, country)
		local success, message = conquer.place_building("mybuilding", session, country, pos)
		if not success then
			minetest.chat_send_player(player:get_player_name(), message)
			return false
		end


		return true
	end,
}
```

### GUI

* `conquer.make_keep_gui(player_name, session, country)`
* `conquer.show_keep_gui(player)`
* `conquer.make_help_formspec(player_name)`
* `conquer.show_help(player_name)`

## Callbacks

* `conquer.register_on_building_placed(function(session, country, building_type))`
  * `building_type` - is one of `keep`, `barracks`.
* `conquer.register_on_building_destroyed(function(session, country, building_type, pos))`
* `conquer.register_on_selected_units_changed(function(player, session, country, unit))`
  * unit is an object ref, may be nil
* `conquer.register_on_join_session(function(player, session))`
* `conquer.register_on_leave_session(function(player, session))`
* `conquer.register_on_change_country(function(player, session, country))`
* `conquer.register_on_can_train_unit(function(session, country, unit_type))`
  * Function can return an error message to prevent the unit from being trained.
* `conquer.register_on_train_unit(function(session, country, unit_type))`
  * Unit has started to be trained, but hasn't finished yet.
* `conquer.register_on_unit_created(function(session, country, unit))`
* `conquer.add_resource(country, stack) --> boolean`
* `conquer.has_resources(country, stacks) --> boolean`
* `conquer.take_resources(country, stacks) --> boolean`


## Session class

A session is a running game. Units and countries from different sessions cannot fight or interact.

* `Session:new(owner, pos)`
* `get_countries() --> table of countries`
* `get_pos() --> position`
* `create_country() --> country`
* `get_country_by_id(id) --> country`
* `get_country_by_player(player_name) --> country`
* `join(player) --> country` - Player joins session, new country created.
* `leave(player)`: leaves session.
* `set_player_country(player, country_id) --> boolean`
* `to_table()` - arbitrary storage table, should be treated as a black box.

## Behaviour Trees

Conquer uses a simple [behaviour tree](https://en.wikipedia.org/wiki/Behavior_tree_%28artificial_intelligence%2C_robotics_and_control%29)
implementation to drive the unit actions.
Behaviour trees can be used to compose complex behaviour,
but their use in conquer is simple.

Composite nodes:

* `FickleSequence:new({child1, child2})` - runs each child every tick.
* `Sequence:new({child1, child2})` - runs each child once, remembering the last ran child.

Action nodes:

* `FollowPath:new(path, object_animations)` - follow a path.
* `MoveToNearTarget:new(target, object_animations)` - move to an object.
* `MeleeAttack:new(target, melee_spec, object_animations)` - attack.
* `RangedAttack:new(target, ranged_spec, object_animations)` - attack.

`target` can be an ObjectRef or a node position.

## Utilities

* `conquer.class() --> table` - makes a Lua class.
  * Add `class.constructor(...)` to handle parameters passed to `:new()`
